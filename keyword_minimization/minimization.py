#!/usr/bin/env python3

import os
import math
from datetime import datetime
from httpget import probe
import argparse

NULL_CHAR = '\x00'
GFW_TESTS = 0

def is_censored(msg, config_file):
    """Test a string or list of strings for censorship.
    :return: True if msg was censored, False otherwise
    """

    test_mode = "all"
    ignore_sanity = True
    config_data = config_file
    outfile = "outfile_" + os.path.basename(config_file)[:-5] + ".log"
    
    global GFW_TESTS
    GFW_TESTS += 1
    is_keyword_blocked = False
    
    print("Probing for censorship: %s"%msg)
    #Test single string
    if(len(msg) == 1):
        for w in msg:
            keyword_array = []
            keyword_array.append(w)

            args = [test_mode, ignore_sanity, keyword_array, config_data, outfile] 
            is_keyword_blocked = probe(args)
            print("Is %s blocked - %s"%(msg,is_keyword_blocked))
    
    #Concat strings to test with the null_char
    if(len(msg) > 1):
        concat_msg = ""

        for w in msg:
            concat_msg = concat_msg + NULL_CHAR + w

        keyword_array = []
        keyword_array.append(concat_msg)

        args = [test_mode, ignore_sanity, keyword_array, config_data, outfile] 
        is_keyword_blocked = probe(args)
        print("Is %s blocked - %s"%(msg,is_keyword_blocked))
    
    return is_keyword_blocked


def bin_search(S, g, config_file):
    """Perform a binary search over g and return the index of the first
    sensitive component in g.
    :param S: set of strings to include with test messages
    :param g: str
    :return: index of first sensitive character in g
    """
    lo, hi = 0, len(g)
    while hi - lo > 1:
        mid = math.floor((lo+hi) / 2)
        if is_censored(S.union({g[mid:]}), config_file):
            lo = mid
        else:
            hi = mid
    return lo


def comp_aware_bin_split(s, config_file):
    """Improved version of bin_split adapted to for identifying keyword combos
    :return: sensitive keyword combination as a set of strings
    """
    C = set()
    while True:
        i = bin_search(C, s, config_file)
        j = i + 1
        k = len(s)
        while j < k:
            if is_censored(C.union({s[i:j], s[i+1:]}), config_file):
                k = j
            else:
                j = j + 1
        C = C.union({s[i:j]})
        if j != len(s):
            s = s[i+1:]
        else:
            s = ""
        if not s or is_censored(C, config_file):
            break
    return C



if __name__ == "__main__":

    log_folder = "minimization_logs"
    if not os.path.exists(log_folder):
        os.makedirs(log_folder)

    parser = argparse.ArgumentParser()
    parser.add_argument('-w', '--wordlist', help='Keywords file path. One keyword per line.',
                    default="wordlists/ultrasurf.txt")
    parser.add_argument('-c', '--config',
                        required=True,
                        help='URL configuration file')
    args = parser.parse_args()

    wordlist = args.wordlist
    wordlist_file = open(wordlist, 'r')
    wordlist_rows = wordlist_file.readlines()
    
    sensitive_keywords = set()
    combos = []

    #Save sensitive keywords to file
    sensitive_keyword_file = open(log_folder + "/sensitive_keywords_" + os.path.basename(args.config)[:-5] + "_" + os.path.basename(wordlist), "w")

    #Start time
    sensitive_keyword_file.write("Start time -- " + datetime.now().strftime("%Y-%m-%d-%Hh%Mm%Ss")+"\n")
    sensitive_keyword_file.write("Sensitive keywords | GFW tests | Original keyword\n")
    sensitive_keyword_file.flush()
    
    for n, row in enumerate(wordlist_rows):
        print("Full string: %s"%row.rstrip())
        print("[#] Keyword %d/%d"%(n+1,len(wordlist_rows)))
        kw = comp_aware_bin_split("KEY"+row.rstrip()+"KEY", args.config)

        # If the keyword in not censored, kw will be the keyword itself
        # So, by prepending and appending a marker (KEY), we can be sure
        #  of whether the enclosed keyword triggered censorship as per the
        #  output stored in kw
        
        print(kw)
        for w in kw:
            if(w != "KEY"+row.rstrip()+"KEY"):
                print("-> Sensitive keyword: %s"%w)
                sensitive_keywords.add(w)
                sensitive_keyword_file.write(w+"|")
                sensitive_keyword_file.flush()
        
        sensitive_keyword_file.write(str(GFW_TESTS) + "|")
        sensitive_keyword_file.flush()
        sensitive_keyword_file.write(row.rstrip() + "\n")
        GFW_TESTS = 0

        
    print("Sensitive keywords found:")
    print(sensitive_keywords)

    #End time
    sensitive_keyword_file.write("End time -- " + datetime.now().strftime("%Y-%m-%d-%Hh%Mm%Ss")+"\n")
    sensitive_keyword_file.flush()
    sensitive_keyword_file.close()
