import sys
import os
import time


def run_minimization_experiment(ks_server_name, wordlist):

    os.system("screen -S keyword-minimization_%s -L -d -m bash -c \"python3 minimization.py -c configs/%s.toml -w wordlists/%s\""%(ks_server_name,ks_server_name,wordlist))


if __name__ == '__main__':
    ks_server_name = sys.argv[1]
    wordlist = sys.argv[2]

    run_minimization_experiment(ks_server_name, wordlist)