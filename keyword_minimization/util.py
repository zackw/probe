import argparse
import asyncio
import base64
import datetime
from http.cookies import BaseCookie, SimpleCookie, Morsel
import os
import socket
import sys
import time

import aiohttp
from aiohttp.abc import AbstractCookieJar
from aiohttp.typedefs import LooseCookies
import idna  # type: ignore
from tqdm import tqdm  # type: ignore
import yarl
from typing import (
    Any,
    Dict,
    Generic,
    IO,
    Iterable,
    Iterator,
    List,
    Mapping,
    Set,
    Tuple,
    TypeVar,
    TYPE_CHECKING,
)

# The type of the object returned by csv.writer() is not exposed by the
# csv module, but it *is* exposed by the typeshed as _csv._writer, so
# this kludge will make both mypy and the regular interpreter happy.
if TYPE_CHECKING:
    from _csv import _writer as CSVWriter
else:
    from csv import writer as CSVWriter

# As of 3.8, io.TextIOWrapper is not a subclass of typing.TextIO, which
# means
#     from typing import TextIO
#     fp = open('foo.txt', 'wt')
#     assert isinstance(fp, TextIO)
# will fail at runtime even though mypy is happy with it.
if TYPE_CHECKING:
    from typing import TextIO
else:
    from io import TextIOBase as TextIO

# mypy understands asyncio.Future to be a generic type, but the regular
# interpreter doesn't, and there is no alias for it in typing.  This is
# only used in type annotations so it doesn't need any detail for the
# regular interpreter.
if TYPE_CHECKING:
    from asyncio import Future as AIOFuture
else:
    class AIOFuture(Generic[TypeVar('T')]):
        pass


__all__ = [
    'AIOFuture',
    'CSVWriter',
    'FALLBACK_UA',
    'MemoizedResolver',
    'OutputFile',
    'ReadOnlyCookieJar',
    'Reporter',
    'TextIO',
    'UnreliableServerError',
    'file2lines',
    'flatten_cert_set',
    'server_certificate',
    'timestamp',
]


def file2lines(filepath: str) -> List[str]:
    """Return a list of all the nonempty lines in the file FILEPATH,
       after stripping leading and trailing whitespace.  Lines whose
       first character is '#' are discarded."""
    lines = []
    with open(filepath, 'rt') as f:
        for line in f:
            line = line.strip()
            if line and line[0] != '#':
                lines.append(line)
    return lines


def flatten_cert_set(certs: Set[bytes]) -> str:
    """Given a set of DER-form certificates, return a printable string
       representation of them.  This will be either the empty string, if
       the set was empty, or a sequence of base64-encoded certificates
       separated by | characters.  The base64 is not line-wrapped, and no
       PEM boundary markers are emitted.
    """
    printable = [
        base64.b64encode(cert).decode("ascii")
        for cert in certs
        if cert
    ]
    printable.sort()
    return "|".join(printable)


def server_certificate(resp: aiohttp.ClientResponse) -> bytes:
    """Retrieve the server certificate associated with an HTTPS response,
       as a binary DER blob. If no certificate is available, returns b''.
    """
    # sometimes we don't get a connection or transport,
    # not sure why
    conn = resp.connection
    if conn is None:
        return b''
    tsp = conn.transport
    if tsp is None:
        return b''
    # get_extra_info('peercert') calls getpeercert(False)
    # which is Not What We Want
    sslob = tsp.get_extra_info('ssl_object')
    blob = sslob.getpeercert(binary_form=True)
    assert isinstance(blob, bytes)  # this is a type hint
    return blob


def timestamp() -> str:
    """Return a human-readable slight variation on an ISO timestamp."""
    return (datetime.datetime.now()
            .replace(microsecond=0)
            .isoformat()
            .replace("T", " "))


def _validate_output_mode(mode: str) -> str:
    append = 0
    read = 0
    write = 0
    excl = 0
    binary = 0
    text = 0
    update = 0
    unewline = 0
    for c in mode:
        if c == "+":
            update += 1
        elif c == "U":
            unewline += 1
        elif c == "a":
            append += 1
        elif c == "b":
            binary += 1
        elif c == "r":
            read += 1
        elif c == "t":
            text += 1
        elif c == "w":
            write += 1
        elif c == "x":
            excl += 1
        else:
            raise ValueError("invalid mode: " + repr(mode))

    if (
            update > 1 or unewline > 1 or append > 1 or binary > 1
            or read > 1 or text > 1 or write > 1 or excl > 1
    ):
        raise ValueError("invalid mode: " + repr(mode))

    if not (append + read + write + excl == 1):
        raise ValueError(
            "must have exactly one of create/read/write/append mode")

    if binary and text:
        raise ValueError("can't have text and binary mode at once")

    if read and not update:
        raise ValueError("read-only mode invalid for output file")

    return (
        "r" if read else "" +
        "w" if write else "" +
        "x" if excl else "" +
        "a" if append else "" +
        "b" if binary else "t" +
        "+" if update else ""
        # in 3.0 'U' no longer has any effect
    )


class OutputFile:
    """Context manager wrapper around opening a file for writing.

       If the arguments request open mode 'x', and the named file
       already exists, retries up to 10000 times with a numeric suffix
       appended to the name.

       If the context is exited by any BaseException *other than*
       KeyboardInterrupt or SystemExit(0), the file will be deleted.
       If it is exited normally, the file will just be closed.
    """

    def __init__(self, fname: str, mode: str, *args: Any, **kwargs: Any):

        mode = _validate_output_mode(mode)
        try:
            self._fp = open(fname, mode, *args, **kwargs)
            self._fname = fname
        except FileExistsError:
            for i in range(10000):
                name = f"{fname}.{i:04d}"
                try:
                    self._fp = open(name, mode, *args, **kwargs)
                    self._fname = name
                    break
                except FileExistsError:
                    pass
            else:
                raise

    def __enter__(self) -> IO[Any]:
        return self._fp.__enter__()

    def __exit__(self, ty: Any, val: Any, tb: Any) -> None:
        if (
                ty is None
                or ty is KeyboardInterrupt
                or ty is SystemExit and val.code == 0
        ):
            # Normal exit.  Close the file and make sure it hits the disk.
            self._fp.flush()
            os.fsync(self._fp.fileno())
            self._fp.close()

        else:
            # Abnormal exit.  Delete the file, ignoring all errors.
            try:
                self._fp.close()
            except OSError:
                pass
            try:
                os.unlink(self._fname)
            except OSError:
                pass


FALLBACK_UA = (
    "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:63.0)"
    " Gecko/20100101 Firefox/63.0"
)


class UnreliableServerError(RuntimeError):
    """Exception thrown when the results for a particular request do
       not converge."""
    pass


class Reporter:
    """Vaguely like a logging.Logger, but much simpler and customized for
       what we want to do in this here program."""
    levels = {
        "fatal": 50,
        "error": 40,
        "warning": 30,
        "info": 20,
        "debug": 10
    }
    FATAL = 50
    ERROR = 40
    WARNING = 30
    INFO = 20
    DEBUG = 10

    @staticmethod
    def add_cli_arguments(parser: argparse.ArgumentParser) -> None:
        parser.add_argument('-C', '--console-log-level',
                            default='warning', choices=Reporter.levels.keys(),
                            help="Level of detail for logging to console")
        parser.add_argument('-l', '--file-log-level',
                            default='info', choices=Reporter.levels.keys(),
                            help="Level of detail for logging to the logfile."
                            " Ignored if -L was not given.")

    def __init__(self, args: argparse.Namespace):
        self.file_level = 30
        self.console_level = 30
        self.errored = False


    def __enter__(self) -> Any:
        self.start_time = self.cycle_start_time = time.monotonic()
        return self

    def __exit__(self, ty: Any, val: Any, tb: Any) -> None:
        # Only call exit_if_errors if we're not already unwinding.
        if ty is None:
            self.exit_if_errors()

    def exit_if_errors(self) -> None:
        if self.errored:
            sys.exit(1)

    def start_cycle(self) -> None:
        self.cycle_start_time = time.monotonic()

    def _report(self, msg: str, level: str) -> None:
        now = time.monotonic()
        elapsed = str(datetime.timedelta(seconds=now - self.start_time))
        celapsed = str(datetime.timedelta(seconds=now - self.cycle_start_time))
        msg = msg.strip()
        nlevel = self.levels[level]
        if nlevel >= self.console_level:
            tqdm.write("{} [{}/{}]: {}".format(level, celapsed, elapsed, msg))

    def _report_exception(self, exc: BaseException,
                          msg: str, level: str) -> None:
        import traceback

        nlevel = self.levels[level]
        if nlevel >= self.console_level or nlevel >= self.file_level:
            err = traceback.format_exception_only(type(exc), exc)[-1].strip()
            if msg:
                msg = f"{msg}: {err}"
            else:
                msg = err
            self._report(msg, level)

    def debug(self, msg: str) -> None:
        self._report(msg, "debug")

    def debug_exception(self, exc: BaseException, msg: str = "") -> None:
        self._report_exception(exc, msg, "debug")

    def info(self, msg: str) -> None:
        self._report(msg, "info")

    def warning(self, msg: str) -> None:
        self._report(msg, "warning")

    def error(self, msg: str) -> None:
        self._report(msg, "error")
        self.errored = True

    def exception(self, exc: BaseException, msg: str = "") -> None:
        self._report_exception(exc, msg, "error")
        self.errored = True

    def fatal(self, msg: str) -> None:
        self._report(msg, "fatal")
        sys.exit(1)

    def fatal_exception(self, exc: BaseException, msg: str = "") -> None:
        self._report_exception(exc, msg, "fatal")
        sys.exit(1)


class MemoizedResolver(aiohttp.abc.AbstractResolver):
    """Wrap another Resolver object and memoize all calls to resolve().
       The resolve_many() function can be used to look up a list of
       hosts all at once.
    """
    def __init__(self,
                 inner: aiohttp.abc.AbstractResolver,
                 reporter: Reporter):
        self._rep = reporter
        self._inner = inner
        self._memo: Dict[Tuple[str, int], List[Dict[str, Any]]] = {}

    async def close(self) -> None:
        await self._inner.close()

    async def resolve(self,
                      host: str,
                      port: int = 0,
                      family: int = socket.AF_INET) -> List[Dict[str, Any]]:
        """Look up the addresses of HOST, in family FAMILY."""

        if family == socket.AF_UNSPEC:
            # Don't put AF_UNSPEC in the memo; look up v4 and v6 addresses
            # separately and concatenate them here.
            rv = await self.resolve(host, port, socket.AF_INET)
            rv.extend(await self.resolve(host, port, socket.AF_INET6))
            return rv

        try:
            ehost = idna.encode(host).decode("ascii")
        except Exception as e:
            self._rep.warning(f"{host}: invalid hostname: {e}")
            return []

        key = (ehost, family)
        if key in self._memo:
            addrs = self._memo[key]
        else:
            addrs = await self._inner_resolve(key, port)

        if not addrs:
            # Special case for the sake of the HTTPS experiments: when
            # a domain name does not resolve to any addresses, check
            # whether the leftmost label (in U-form) has dashes in it.
            # if it does, delete the characters from the rightmost
            # dash to the end of the label, and if that produces a name
            # that does resolve to addresses, use those addresses for this
            # name as well.  For instance, assuming 'paris.domain.example'
            # has an A record but 'paris-ultrasurf.domain.example' does not,
            # this resolver will behave as if 'paris-ultrasurf.domain.example'
            # has the same A record as 'paris.domain.example'.
            labels = host.split('.')
            if '-' in labels[0]:
                labels[0] = '-'.join(labels[0].split('-')[:-1])
                addrs = await self.resolve('.'.join(labels), port, family)
                if addrs:
                    self._memo[key] = addrs

        def subst_port(addr: Dict[str, Any], port: int) -> Dict[str, Any]:
            rv = addr.copy()
            rv['port'] = port
            return rv

        return [subst_port(a, port) for a in addrs]

    async def _inner_resolve(self, key: Tuple[str, int],
                             port: int = 0) -> List[Dict[str, Any]]:
        """Call the inner resolver and write the results to the memo.
           Catch all exceptions, log them, and memoize the failure."""
        try:
            addrs = await self._inner.resolve(key[0], port, key[1])
        except Exception as e:
            addrs = []

            host = key[0]
            if "xn--" in host:
                try:
                    host = f"{idna.decode(host)} ({host})"
                except Exception:
                    pass

            fam = "[IPv4]" if key[1] == socket.AF_INET else "[IPv6]"
            self._rep.info(f"{host} {fam}: {e}")

        self._memo[key] = addrs
        return addrs

    async def resolve_many(self, hosts: Iterable[str]) -> Set[str]:
        """Resolve both IPv4 and IPv6 addresses for all of the hosts in
           HOSTS immediately.  Return the subset of those hosts which
           had at least one valid address."""

        good_hosts = set()
        tasks = []
        families = [socket.AF_INET, socket.AF_INET6]
        for h in hosts:
            try:
                eh = idna.encode(h).decode("ascii")
            except Exception as e:
                self._rep.warning(f"{h}: invalid hostname: {e}\n")
                continue

            for f in families:
                key = (eh, f)
                if key not in self._memo:
                    tasks.append(asyncio.ensure_future(
                        self._inner_resolve(key)))

        with tqdm(total=len(tasks), desc="DNS resolution",
                  unit=" records") as bar:
            for fut in asyncio.as_completed(tasks):
                addrs = await fut
                if addrs:
                    good_hosts.add(addrs[0]['hostname'])
                bar.update(1)

        return good_hosts


class ReadOnlyCookieJar(AbstractCookieJar):
    """Cookie jar that stores the cookies specified at creation and
       silently ignores any attempt to set more cookies."""

    def __init__(self, *, cookies: Mapping[str, str]):
        super().__init__()
        self._cookies: SimpleCookie[str] = SimpleCookie()
        for name, value in cookies.items():
            self._cookies[name] = value

    def clear(self) -> None:
        pass

    def update_cookies(self,
                       cookies: LooseCookies,
                       response_url: yarl.URL = yarl.URL()) -> None:
        pass

    def filter_cookies(self, request_url: yarl.URL) -> 'BaseCookie[str]':
        return self._cookies

    def __iter__(self) -> 'Iterator[Morsel[str]]':
        return iter(self._cookies.values())

    def __len__(self) -> int:
        return len(self._cookies)
