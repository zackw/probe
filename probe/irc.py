"""Probe for censored keywords over IRC."""

import argparse
import socket
import sys
import time

NICK = 'soetiaaffxn'
CHAN = f'{NICK}Chan'

PACKETS = [
    f'NICK {NICK}\n'.encode('utf-8'),
    f'USER {NICK} 0 * {NICK}\n'.encode('utf-8'),
    b'\n',
    f'JOIN #{CHAN}\n'.encode('utf-8'),
    f'PRIVMSG #{CHAN} "足球法轮多维盘古乐队无界花园网华夏文摘动态网延安日记博讯世界经济导报"\n'.encode('utf-8'),
    'HELP\n'.encode('utf-8'),
]

BUF_SIZE = 1024


def recv_all(sock: socket.socket) -> None:
    while True:
        data = sock.recv(BUF_SIZE)
        print(data.decode('utf-8', errors='ignore'))
        if len(data) < BUF_SIZE:
            break
    print('<< ALL RECEIVED >>')


def probe(args: argparse.Namespace) -> None:
    try:
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    except socket.error:
        print('Failed to create socket')
        sys.exit()

    try:
        s.connect((args.addr, args.port))
    except socket.gaierror:
        print(f'Connect to {args.addr}:{args.port} failed.')
        sys.exit()

    try:
        time.sleep(3)
        for p in PACKETS:
            recv_all(s)
            print('Send> ', p.decode())
            s.send(p)
            time.sleep(0.5)
        recv_all(s)
        print('done')
    except Exception as e:
        print(e)
    finally:
        s.close()


def init_subparser(parser: argparse.ArgumentParser) -> None:
    parser.add_argument('-s', '--server', dest='addr', help='Server address')
    parser.add_argument('-p', '--port', dest='port',
                        default=6667,
                        help='Telnet server port. Default 6667')
