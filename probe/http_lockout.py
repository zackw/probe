"""Probe for a lockout on all HTTP requests after a censored request."""

import argparse
import asyncio
import csv
import random
import time

from typing import List, Optional, Tuple

from .util import (
    CSVWriter, FALLBACK_UA, MemoizedResolver, OutputFile, Reporter,
)

import aiohttp
import toml
from tqdm import tqdm  # type: ignore


class Config:
    def __init__(self, args: argparse.Namespace, rep: Reporter):
        self.test_cycles = args.cycles

        configfile = args.config_file
        try:
            data = toml.load(configfile)
        except Exception as e:
            rep.fatal(f"{configfile}: parse error: {e}")

        try:
            urls = data['urls']
            servers = urls['servers']
            self.templates: List[str] = urls['templates']
            self.benign = urls['benign']
            self.filtered = urls['filtered']

            request = data['request']
            self.interval = request['interval']
            self.timeout = request['timeout']
            self.successes = request['successes']
            self.abandon_after = request['abandon_after']

            self.add_headers = request['headers']

        except Exception as e:
            rep.fatal(
                f"{configfile}: required field missing or invalid: {e}")

        self.rng = random.Random()

        self.server_ports = {}
        for server in servers:
            if not server:
                rep.error(f"{configfile}: {server}: empty name is invalid")
                continue

            # What about IPv6 address literals?
            if ':' in server:
                host, port = server.split(':')
                try:
                    port = int(port)
                except ValueError:
                    rep.error(f"{configfile}: {server}: bad port number")
                    continue
                if not (1 <= port <= 65535):
                    rep.error(f"{configfile}: {server}: bad port number")
                    continue
            else:
                host = server
                port = 80

            self.server_ports[host] = port

    def choose_template(self, server: str, keyword: str) -> str:
        port = self.server_ports[server]
        if port != 80:
            server += f":{port}"
        return self.rng.choice(self.templates).format(
            server=server,
            keyword=keyword)

    def choose_benign(self, server: str) -> str:
        return self.choose_template(server, self.rng.choice(self.benign))

    def choose_filtered(self, server: str) -> str:
        return self.choose_template(server, self.rng.choice(self.filtered))

    async def preresolve_servers(self, resolver: MemoizedResolver) -> None:
        all_servers = list(self.server_ports.keys())
        good_servers = await resolver.resolve_many(all_servers)
        for s in all_servers:
            if s not in good_servers:
                del self.server_ports[s]

        self.servers = set(self.server_ports.keys())


async def http_request(server: str, filtered: bool,
                       session: aiohttp.ClientSession,
                       config: Config,
                       rep: Reporter) -> Tuple[str, bool]:
    if filtered:
        url = config.choose_filtered(server)
    else:
        url = config.choose_benign(server)

    try:
        rep.info(f"GET {url}...")
        async with session.get(url) as resp:
            await resp.read()

            rep.info(f"GET {url}: {resp.status} {resp.reason}")
            return (server, resp.status == 200)

    except Exception as e:
        rep.info(f"GET {url}: {e}")
        return (server, False)


async def run_benign_probes(
        label: str,
        wr: Optional[CSVWriter],
        cycle: Optional[int],
        locked_host: Optional[str],
        session: aiohttp.ClientSession,
        config: Config,
        rep: Reporter) -> List[str]:

    limit = config.abandon_after
    interval = config.interval
    successes_required = config.successes
    servers_remaining = {
        s: successes_required
        for s in config.servers
    }

    with tqdm(total=limit, desc=label, unit="s", leave=False) as bar:
        start = start_cycle = time.monotonic()
        elapsed = 0.
        while elapsed < limit and servers_remaining:
            tasks = [
                http_request(s,
                             filtered=False,
                             session=session,
                             config=config,
                             rep=rep)
                for s in servers_remaining.keys()
            ]
            for fut in tqdm(asyncio.as_completed(tasks),
                            total=len(config.servers),
                            desc=f"after {elapsed:0.2f}s",
                            leave=False):
                server, ok = await fut
                if wr is not None:
                    wr.writerow((cycle, locked_host, server, elapsed, not ok))
                if ok:
                    servers_remaining[server] -= 1
                    if servers_remaining[server] <= 0:
                        del servers_remaining[server]
                else:
                    servers_remaining[server] = successes_required

            pelapsed = time.monotonic() - start_cycle
            bar.update(pelapsed)

            delay = interval - pelapsed
            if delay > 0:
                await asyncio.sleep(delay)
                bar.update(delay)

            start_cycle = time.monotonic()
            elapsed = start_cycle - start

    return sorted(servers_remaining.keys())


async def test_cycle(wr: CSVWriter,
                     cycle: int,
                     server: str,
                     session: aiohttp.ClientSession,
                     config: Config,
                     rep: Reporter) -> None:

    rep.start_cycle()

    # Make a request that should trigger the filter.
    (_, ok) = await http_request(server, filtered=True,
                                 session=session, config=config, rep=rep)
    # That request should have *failed*.
    if ok:
        rep.warning(f"filtered request to {server} did not fail")
        return

    wr.writerow((cycle, server, server, 0, True))

    # Run benign queries to all of the servers until either we can
    # communicate with all of them again, or the `abandon_after` interval
    # has elapsed.
    failed = await run_benign_probes(f"Lockout from {server}",
                                     wr, cycle, server,
                                     session, config, rep)

    # If we didn't get all of the servers back, stop using the ones that
    # we didn't get back.
    for bad_server in failed:
        rep.warning(f"not testing {bad_server} anymore")
        config.servers.remove(bad_server)


async def probe_task(wr: CSVWriter,
                     config: Config,
                     rep: Reporter) -> None:

    resolver = MemoizedResolver(aiohttp.resolver.DefaultResolver(), rep)
    try:
        await config.preresolve_servers(resolver)
        if not config.servers:
            rep.fatal("No usable servers, exiting.")

        timeout = aiohttp.ClientTimeout(total=config.timeout)
        headers = config.add_headers.copy()
        if 'User-Agent' not in headers:
            headers['User-Agent'] = FALLBACK_UA

        async with aiohttp.ClientSession(
            timeout=timeout,
            headers=headers,
            cookie_jar=aiohttp.DummyCookieJar(),
            connector=aiohttp.TCPConnector(resolver=resolver)
        ) as session:

            initial_failed = await run_benign_probes("Check servers",
                                                     None, None, None,
                                                     session, config, rep)
            for bad_server in initial_failed:
                rep.warning(f"not testing {bad_server}")
                config.servers.remove(bad_server)

            for cycle in tqdm(range(config.test_cycles), desc="Test cycle"):
                with tqdm(config.servers, unit="server", leave=False) as bar:
                    for server in bar:
                        bar.set_description("probing " + server)
                        await test_cycle(wr, cycle, server,
                                         session, config, rep)

    finally:
        await resolver.close()


def probe(args: argparse.Namespace) -> None:
    with Reporter(args) as rep:
        config = Config(args, rep)
        rep.exit_if_errors()

        try:
            with OutputFile(args.out_file, "xt") as outf:
                wr = csv.writer(outf, dialect='unix',
                                quoting=csv.QUOTE_MINIMAL)
                wr.writerow(("cycle", "locked.host", "host",
                             "elapsed", "blocked"))

                loop = asyncio.get_event_loop()
                try:
                    loop.run_until_complete(probe_task(wr, config, rep))

                finally:
                    loop.run_until_complete(loop.shutdown_asyncgens())
                    loop.close()

                rep.exit_if_errors()

        except Exception as e:
            rep.fatal_exception(e)


def init_subparser(parser: argparse.ArgumentParser) -> None:
    def posint(arg: str) -> int:
        val = int(arg)
        if val <= 0:
            raise ValueError("value must be positive")
        return val

    parser.add_argument('config_file', help='URL configuration file')
    parser.add_argument('out_file', help='File to write results to')
    parser.add_argument('-c', '--cycles', type=posint, default=1,
                        help="Repeat the test this many times")
    Reporter.add_cli_arguments(parser)
