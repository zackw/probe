"""Probe for censored keywords over HTTP."""

import argparse
import asyncio
import collections
import csv
import os
import random
import ssl
import time
from urllib.parse import urlsplit, quote as urlquote
from typing import (
    Iterable, Tuple, Set,
)

from .util import (
    CSVWriter,
    CertCapturingClientResponse,
    ENCODINGS,
    FALLBACK_UA,
    MemoizedResolver,
    OutputFile,
    ReadOnlyCookieJar,
    Reporter,
    file2lines,
    flatten_cert_set,
)

import aiohttp
import toml
from tqdm import tqdm  # type: ignore


class UnreliableServerError(RuntimeError):
    """Exception thrown when the results for a particular request do
       not converge."""
    pass


class Config:
    def __init__(self,
                 args: argparse.Namespace,
                 rep: Reporter):

        self.test_mode = args.mode
        self.ignore_sanity = args.ignore_sanity

        try:
            self.words = file2lines(args.keywordfile)
        except Exception as e:
            rep.fatal_exception(e, f"{args.keywordfile}: parse error")

        try:
            data = toml.load(args.configfile)
        except Exception as e:
            rep.fatal_exception(e, f"{args.configfile}: parse error")

        try:
            self.urls = data['urls']

            rc = data['request']
            self.headers = rc['headers']
            if 'User-Agent' not in self.headers:
                self.headers['User-Agent'] = FALLBACK_UA

            self.cookies = rc['cookies']
            self.interval = rc.get('interval', 0.1)
            self.lockout_interval = rc.get('lockout-interval', 1)
            self.abandon_after = rc.get('abandon-after', 300)
            self.successes = rc.get('successes', 10)
            self.timeout = rc.get('timeout', 5)
            self.timeout_retries = rc.get('timeout-retries', 10)

        except Exception as e:
            rep.fatal_exception(
                e, f"{args.configfile}: required field missing or invalid")

        # use typical SSL settings, but don't authenticate the server
        # (we will instead log the certificate we get).
        self.ssl_ctx = ssl.create_default_context()
        self.ssl_ctx.check_hostname = False
        self.ssl_ctx.verify_mode = ssl.CERT_NONE

    async def preresolve_sites(self,
                               resolver: MemoizedResolver,
                               rep: Reporter) -> None:
        all_sites = set()
        for group in self.urls.values():
            for url in group:
                all_sites.add(urlsplit(url).hostname)

        good_sites = await resolver.resolve_many(all_sites)
        for bad_site in all_sites - good_sites:
            rep.error(f"{bad_site}: unable to resolve host")

    def test_urls(self) -> Iterable[Tuple[str, str, str]]:
        check_urls = self.urls["check"]

        for w in self.words:
            # We want to yield the items of this dictionary in the
            # order they were inserted (matching the order of
            # ENCODINGS).  In 3.6 we can't count on iteration
            # order of a plain dict to be insertion order.
            variants = collections.OrderedDict()  # type: ignore
            for encoding in ENCODINGS:
                try:
                    ew = urlquote(w, encoding=encoding)
                    variants.setdefault(ew, encoding)
                except UnicodeError:
                    # keyword can't be represented in that encoding
                    pass

            if self.test_mode == "one":
                url = random.choice(check_urls)
                for ew, encoding in variants.items():
                    yield (w, encoding, url.format(keyword=ew))
            else:
                assert self.test_mode == "all"
                for u in check_urls:
                    for ew, encoding in variants.items():
                        yield (w, encoding, u.format(keyword=ew))


async def try_httpget(url: str,
                      config: Config,
                      session: aiohttp.ClientSession,
                      rep: Reporter,
                      want_certificate: bool) -> Tuple[bool, str]:
    for trial in range(config.timeout_retries):
        try:
            cert = ''
            async with session.get(url, ssl=config.ssl_ctx) as resp:
                if want_certificate and url[:5].lower() == "https":
                    assert isinstance(resp, CertCapturingClientResponse)
                    cert = resp.server_certificate

                # consume and discard the full response
                await resp.read()

                # treat Method Not Allowed and Bad Request as
                # "successful" since we know they're coming from the
                # real server in response to our dodgy requests
                ok = resp.status in (200, 400, 405)

                rep.debug(f"GET {url}: {resp.status} {resp.reason}")
                return (ok, cert)

        except asyncio.TimeoutError:
            rep.debug(f"GET {url}: timeout {trial+1}/{config.timeout_retries}")
            continue

        except OSError as e:
            # don't log a traceback for OSError, various network errors
            # (most importantly ECONNRESET) are expected
            rep.debug(f"GET {url}: {e}")
            return (False, '')

        except Exception as e:
            rep.debug_exception(e, f"GET {url}")

    # too many timeouts = failure
    return (False, '')


async def lockout(config: Config,
                  session: aiohttp.ClientSession,
                  rep: Reporter) -> None:
    limit = config.abandon_after
    interval = config.lockout_interval
    successes_required = config.successes
    urls = config.urls['benign']

    start = start_cycle = time.monotonic()
    elapsed = 0.
    successes = 0
    while elapsed < limit:
        ok, _ = await try_httpget(random.choice(urls), config, session, rep,
                                  want_certificate=False)
        if ok:
            successes += 1
        else:
            successes = 0

        if successes >= successes_required:
            return

        pelapsed = time.monotonic() - start_cycle
        delay = interval - pelapsed
        if delay > 0:
            await asyncio.sleep(delay)

        start_cycle = time.monotonic()
        elapsed = start_cycle - start

    raise UnreliableServerError


async def sanity_check(config: Config,
                       session: aiohttp.ClientSession,
                       rep: Reporter) -> bool:
    urls = config.urls
    good_ok, _ = await try_httpget(random.choice(urls['benign']),
                                   config, session, rep,
                                   want_certificate=False)

    bad_ok, _ = await try_httpget(random.choice(urls['sanity-check']),
                                  config, session, rep,
                                  want_certificate=False)

    return good_ok and not bad_ok


async def probe_keyword(url: str,
                        config: Config,
                        session: aiohttp.ClientSession,
                        rep: Reporter) -> Tuple[bool, Set[str]]:
    limit = config.abandon_after
    interval = config.interval
    successes_required = config.successes

    start = start_cycle = time.monotonic()
    elapsed = 0.0
    successes = 0
    failures = 0
    certificates = set()
    while elapsed < limit:
        ok, cert = await try_httpget(url, config, session, rep,
                                     want_certificate=True)
        certificates.add(cert)
        if ok:
            successes += 1
            failures = 0
        else:
            successes = 0
            failures += 1

        if successes >= successes_required:
            return (True, certificates)
        if failures >= successes_required:
            return (False, certificates)

        pelapsed = time.monotonic() - start_cycle
        delay = interval - pelapsed
        if delay > 0:
            await asyncio.sleep(delay)

        start_cycle = time.monotonic()
        elapsed = start_cycle - start

    raise UnreliableServerError


async def probe_all_keywords(wr: CSVWriter, config: Config,
                             rep: Reporter) -> None:

    resolver = MemoizedResolver(aiohttp.resolver.DefaultResolver(), rep)
    try:
        await config.preresolve_sites(resolver, rep)
        rep.exit_if_errors()

        async with aiohttp.ClientSession(
                timeout=aiohttp.ClientTimeout(total=config.timeout),
                headers=config.headers,
                cookie_jar=ReadOnlyCookieJar(cookies=config.cookies),
                connector=aiohttp.TCPConnector(resolver=resolver,
                                               use_dns_cache=False),
                response_class=CertCapturingClientResponse
        ) as session:

            test_urls = list(config.test_urls())
            random.shuffle(test_urls)
            filtered = 0
            with tqdm(
                    test_urls, unit="word",
                    bar_format="{desc:.20}{percentage:3.0f}%|{bar}{r_bar}"
            ) as bar:

                bar.set_description("sanity check")
                if not (await sanity_check(config, session, rep)):
                    if config.ignore_sanity:
                        rep.warning("Sanity check failed, continuing anyway.")
                    else:
                        rep.error("Sanity check failed, aborting.")
                        return

                await lockout(config, session, rep)

                for word, encoding, url in bar:
                    bar.set_description(f"testing {word} ({encoding})")
                    ok, certs = await probe_keyword(url, config, session, rep)
                    wr.writerow((word, encoding, not ok, url,
                                 flatten_cert_set(certs)))
                    if not ok:
                        filtered += 1
                        bar.set_postfix(filtered=filtered)
                        bar.set_description("lockout")
                        await lockout(config, session, rep)

    except UnreliableServerError:
        rep.fatal("abandoning test: responses unreliable")

    finally:
        await resolver.close()


def probe(args: argparse.Namespace) -> None:
    with Reporter(args) as rep:
        config = Config(args, rep)
        rep.exit_if_errors()

        outfile = args.outfile
        if not outfile:
            outfile = (os.path.splitext(os.path.basename(args.configfile))[0]
                       + ".log")

        try:
            with OutputFile(outfile, "xt") as outf:
                wr = csv.writer(outf, dialect='unix',
                                quoting=csv.QUOTE_MINIMAL)
                wr.writerow(("word", "encoding", "censored", "url", "certs"))

                loop = asyncio.get_event_loop()
                try:
                    loop.run_until_complete(
                        probe_all_keywords(wr, config, rep))
                finally:
                    loop.run_until_complete(loop.shutdown_asyncgens())
                    loop.close()

                rep.exit_if_errors()

        except Exception as e:
            rep.fatal_exception(e)


def init_subparser(parser: argparse.ArgumentParser) -> None:
    parser.add_argument('-k', '--keywords', dest='keywordfile',
                        required=True,
                        help='Keywords file path. One keyword per line.')
    parser.add_argument('-o', '--out', dest='outfile',
                        default=None,
                        help='Output path')
    parser.add_argument('-c', '--config', dest='configfile',
                        required=True,
                        help='URL configuration file')
    parser.add_argument('-i', '--ignore-sanity', dest='ignore_sanity',
                        default=False, action='store_true',
                        help='Continue even if the sanity test fails')
    parser.add_argument('-m', '--mode', dest='mode', default="one",
                        choices=("one", "all"),
                        help="Whether to test each keyword with one or all"
                        " of the configured test URLs.")

    Reporter.add_cli_arguments(parser)
