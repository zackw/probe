"""Probe for the ports that are inspected for forbidden HTTP requests."""

import argparse
import asyncio
import csv
import ssl
import sys
import time

from itertools import zip_longest
from urllib.parse import SplitResult, quote as urlquote

from typing import (
    Dict,
    Sequence,
    List,
    Tuple,
)

from .util import (
    CertCapturingClientResponse,
    MemoizedResolver,
    Reporter,
    TextIO,
    non_checking_client_ssl_context,
)

import aiohttp
from tqdm import tqdm  # type: ignore


#: number of parallel requests per host
PARALLEL = 32

#: seconds to wait between consecutive requests to the same host
INTERVAL = 0.1

#: overall timeout, in seconds, for any one request
TIMEOUT = 5

#: we must receive this many consecutive successful requests before we
#: believe there is no censorship happening
SUCCESSES_REQUIRED = 10

#: if we don't get enough successes or failures in this amount of time
#: (seconds) we give up
LIMIT = 300

#: URL template (echo server behavior)
PATH_TEMPLATE = "/whatisthis?k={keyword}"

#: These invariant HTTP headers mimic Firefox 68.0ESR (Windows) with
#: zh_CN localization as closely as possible.
INVARIANT_HEADERS = {
    "User-Agent": ("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:68.0)"
                   " Gecko/20100101 Firefox-75.0"),
    "Accept": ("text/html,application/xhtml+xml,application/xml;q=0.9"
               ",image/webp,*/*;q=0.8"),
    "Accept-Language": ("zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5"
                        ",en-US;q=0.3,en;q=0.2"),
    "Upgrade-Insecure-Requests": "1",
}


def chunked(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks"
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)


def construct_probe_url(port: int, args: argparse.Namespace) -> str:
    scheme = args.protocol
    host = args.host
    query = "k=" + urlquote(args.blocked_keyword, encoding="utf-8")

    if scheme == "https":
        # put the censored keyword in the Host: header and the
        # SNI as well as the path
        labels = host.split(".")
        labels[0] = labels[0] + "-" + (args.blocked_keyword
                                       .replace("_", "-")
                                       .replace(" ", "-")
                                       .replace(".", "-"))
        host = ".".join(labels)

    if (
            (scheme == "http" and port == 80)
            or (scheme == "https" and port == 443)
    ):
        netloc = host
    else:
        netloc = host + ":" + str(port)

    return SplitResult(
        scheme=scheme,
        netloc=netloc,
        path="/whatisthis",
        query=query,
        fragment=""
    ).geturl()


async def send_http_request(
        url: str,
        session: aiohttp.ClientSession,
        ssl_ctx: ssl.SSLContext,
        rep: Reporter) -> Tuple[bool, str]:

    try:
        async with session.request("GET", url, ssl=ssl_ctx) as resp:

            if url[:5].lower() == "https":
                assert isinstance(resp, CertCapturingClientResponse)
                cert = resp.server_certificate
            else:
                cert = ""

            # consume and discard the full response
            await resp.read()

            # treat Method Not Allowed and Bad Request as "successful"
            # since we know they're coming from the real server in response
            # to our dodgy requests
            ok = resp.status in (200, 400, 405)

            rep.debug(f"GET {url}: {resp.status} {resp.reason}")
            return (ok, cert)

    except Exception as e:
        if isinstance(e, asyncio.TimeoutError):
            rep.debug(f"GET {url}: timeout")
        else:
            rep.debug(f"GET {url}: {e}")
            if not isinstance(e, OSError):
                rep.debug_exception(e)

        return (False, "")


async def probe_one_port(port: int,
                         rep: Reporter,
                         session: aiohttp.ClientSession,
                         ctx: ssl.SSLContext,
                         args: argparse.Namespace) -> Dict[str, str]:

    limit = LIMIT
    interval = INTERVAL
    successes_required = SUCCESSES_REQUIRED

    probe_url = construct_probe_url(port, args)
    start = start_cycle = time.monotonic()
    elapsed = 0.
    successes = 0
    failures = 0
    certificates = set()
    while elapsed < limit:
        (ok, cert) = await send_http_request(probe_url, session, ctx, rep)
        certificates.add(cert)
        if ok:
            successes += 1
            failures = 0
        else:
            successes = 0
            failures += 1

        if successes >= successes_required or failures >= successes_required:
            break

        pelapsed = time.monotonic() - start_cycle
        delay = interval - pelapsed
        if delay > 0:
            await asyncio.sleep(delay)

        start_cycle = time.monotonic()
        elapsed = start_cycle - start

    if successes >= successes_required:
        status = "open"
    elif failures >= successes_required:
        status = "censored"
    else:
        status = "uncertain"

    return {
        "port": format(port, ">05d"),
        "status": status,
        "certs": "|".join(sorted(certificates))
    }


async def probe_async_batch(results: asyncio.Queue,
                            ports: Sequence[int],
                            rep: Reporter,
                            session: aiohttp.ClientSession,
                            ctx: ssl.SSLContext,
                            args: argparse.Namespace) -> None:
    for port in ports:
        if port > 0:
            await results.put(
                await probe_one_port(port, rep, session, ctx, args)
            )


async def probe_async_output(outf: TextIO,
                             results: asyncio.Queue,
                             todo: int) -> None:

    rows: List[Dict[str, str]] = []
    with tqdm(desc="probing", unit="port", total=todo) as bar:
        while len(rows) < todo:
            rows.append(await results.get())
            bar.update(1)

    rows.sort(key=lambda r: r["port"])

    wr = csv.DictWriter(outf, fieldnames=[
        "port", "status", "certs"
    ], dialect="unix", quoting=csv.QUOTE_MINIMAL)
    wr.writeheader()
    wr.writerows(rows)


async def probe_async(outf: TextIO,
                      rep: Reporter,
                      args: argparse.Namespace) -> None:

    # use typical SSL settings, but don't authenticate the server
    # (we will instead log the certificate we get).
    ctx = non_checking_client_ssl_context()

    resolver = MemoizedResolver(aiohttp.resolver.DefaultResolver(), rep)
    try:
        good = await resolver.resolve_many([args.host])
        if not good:
            rep.fatal(f"could not resolve address of {args.host}")

        async with aiohttp.ClientSession(
                timeout=aiohttp.ClientTimeout(total=TIMEOUT),
                headers=INVARIANT_HEADERS,
                cookie_jar=aiohttp.DummyCookieJar(),
                connector=aiohttp.TCPConnector(resolver=resolver,
                                               use_dns_cache=False),
                response_class=CertCapturingClientResponse
        ) as session:

            rq: 'asyncio.Queue[Dict[str, str]]' = asyncio.Queue()
            tasks = []
            all_ports = range(args.first_port, args.last_port + 1)
            todo = len(all_ports)
            batchsize, rem = divmod(todo, PARALLEL)
            if rem:
                batchsize += 1

            for ports in chunked(all_ports, batchsize, 0):
                tasks.append(asyncio.ensure_future(
                    probe_async_batch(rq, ports, rep, session, ctx, args)
                ))

            tasks.append(asyncio.ensure_future(
                probe_async_output(outf, rq, todo)
            ))

            await asyncio.gather(*tasks)

    finally:
        await resolver.close()


def probe(args: argparse.Namespace) -> None:
    with Reporter(args) as rep:
        try:
            with sys.stdout as outf:
                loop = asyncio.get_event_loop()
                try:
                    loop.run_until_complete(probe_async(outf, rep, args))
                finally:
                    loop.run_until_complete(loop.shutdown_asyncgens())
                    loop.close()

        except Exception as e:
            rep.fatal_exception(e)


def tcp_port(arg: str) -> int:
    """Argparse type check function: If STR represents a valid
       TCP port (that is, if 1 <= int(arg) <= 65535)) then return
       int(arg), else raise ValueError.
    """
    val = int(arg)
    if 1 <= val <= 65535:
        return val
    raise ValueError(f"{arg!r} is not a valid TCP port number")


def init_subparser(parser: argparse.ArgumentParser) -> None:
    parser.add_argument("host", help="Host to probe")
    parser.add_argument("first_port", type=tcp_port,
                        help="First TCP port to probe.")
    parser.add_argument("last_port", type=tcp_port,
                        help="Last TCP port to probe.")
    parser.add_argument("protocol", choices=("http", "https"),
                        help="Protocol to use.")
    parser.add_argument("blocked_keyword",
                        help="Keyword to include in each HTTP request."
                        " Pick a term that is known to be censored.")

    Reporter.add_cli_arguments(parser)
