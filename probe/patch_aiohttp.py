#
# Monkey-patching aiohttp to support pre-encoded URLs and HTTP request
# headers better.
#
# This file intentionally pulls all sorts of dynamic-typing shenanigans;
# type checking it would not be useful.
#
# type: ignore

import urllib.parse
from urllib.parse import SplitResult, SplitResultBytes, urlsplit

import aiohttp
from multidict import CIMultiDict, MultiDict, MultiDictProxy
import yarl


__all__ = ['patch_aiohttp_for_byte_urls']

# yarl.URL tries to stop you from subclassing it.
del yarl.URL.__init_subclass__


class ByteURL(yarl.URL):
    def __new__(cls, val="", *, encoded=True):
        if isinstance(val, str) or isinstance(val, SplitResult):
            return super().__new__(val, encoded=encoded)
        elif isinstance(val, bytes):
            val = urlsplit(val)
        elif isinstance(val, SplitResultBytes):
            pass
        else:
            raise ValueError(
                "Constructor parameter should be str, bytes, or SplitResult")

        if not encoded:
            raise ValueError("Byte-URLs must already be encoded")

        self = object.__new__(cls)
        self._val = val
        self._cache = {}
        return self

    def __str__(self):
        return super().__str__().decode("ascii", "backslashreplace")

    def with_fragment(self, fragment):
        if fragment is None:
            raw_fragment = b""
        elif not isinstance(fragment, bytes):
            raise TypeError("invalid fragment type")
        else:
            raw_fragment = fragment
        if self.raw_fragment == raw_fragment:
            return self
        return ByteURL(self._val._replace(fragment=raw_fragment))

    @property
    def scheme(self):
        return self._val.scheme.decode("ascii")

    @property
    def host(self):
        return self.raw_host.decode("utf-8")


def _patched_serialize_headers(status_line, headers):
    data = bytearray()
    if isinstance(status_line, bytes):
        data.extend(status_line)
    else:
        data.extend(status_line.encode("utf-8"))

    data.extend(b'\r\n')
    for k, v in headers.items():
        data.extend(k if isinstance(k, bytes) else k.encode('ascii'))
        data.extend(b': ')
        data.extend(v if isinstance(v, bytes) else v.encode('utf-8'))
        data.extend(b'\r\n')
    data.extend(b'\r\n')
    return bytes(data)


class PatchedClientRequest(aiohttp.client_reqrep.ClientRequest):
    def update_headers(self, headers):
        """Update request headers."""
        self.headers = CIMultiDict()  # type: CIMultiDict[str]

        # add host
        netloc = self.url.raw_host
        if isinstance(netloc, bytes):
            netloc = netloc.decode("utf-8")
        else:
            assert isinstance(netloc, str)
        if aiohttp.helpers.is_ipv6_address(netloc):
            netloc = '[{}]'.format(netloc)
        if self.url.port is not None and not self.url.is_default_port():
            netloc += ':' + str(self.url.port)
        self.headers[aiohttp.hdrs.HOST] = netloc

        if headers:
            if isinstance(headers, (dict, MultiDictProxy, MultiDict)):
                headers = headers.items()

            for key, value in headers:
                # A special case for Host header
                if key.lower() == 'host':
                    self.headers[key] = value
                else:
                    self.headers.add(key, value)

    async def send(self, conn):
        assert self.method != aiohttp.hdrs.METH_CONNECT
        assert not self.proxy

        path = self.url.raw_path
        if self.url.raw_query_string:
            if isinstance(path, str):
                path += '?' + self.url.raw_query_string
            else:
                path += b'?' + self.url.raw_query_string

        protocol = conn.protocol
        assert protocol is not None
        writer = aiohttp.http.StreamWriter(
            protocol, self.loop,
            on_chunk_sent=self._on_chunk_request_sent
        )

        if self.compress:
            writer.enable_compression(self.compress)

        if self.chunked is not None:
            writer.enable_chunking()

        # set default content-type
        if (self.method in self.POST_METHODS and
                aiohttp.hdrs.CONTENT_TYPE not in self.skip_auto_headers and
                aiohttp.hdrs.CONTENT_TYPE not in self.headers):
            self.headers[aiohttp.hdrs.CONTENT_TYPE] = \
                'application/octet-stream'

        # set the connection header
        connection = self.headers.get(aiohttp.hdrs.CONNECTION)
        if not connection:
            if self.keep_alive():
                if self.version == aiohttp.http.HttpVersion10:
                    connection = 'keep-alive'
            else:
                if self.version == aiohttp.http.HttpVersion11:
                    connection = 'close'

        if connection is not None:
            self.headers[aiohttp.hdrs.CONNECTION] = connection

        # status + headers
        status_line = (
            self.method.encode("ascii")
            + b' '
            + (path if isinstance(path, bytes) else path.encode('utf-8'))
            + b' HTTP/'
            + str(self.version[0]).encode("ascii")
            + b'.'
            + str(self.version[1]).encode("ascii")
        )
        await writer.write_headers(status_line, self.headers)

        self._writer = self.loop.create_task(self.write_bytes(writer, conn))

        response_class = self.response_class
        assert response_class is not None
        self.response = response_class(
            self.method, self.original_url,
            writer=self._writer, continue100=self._continue, timer=self._timer,
            request_info=self.request_info,
            traces=self._traces,
            loop=self.loop,
            session=self._session
        )
        return self.response


def patch_aiohttp_for_byte_urls() -> None:
    aiohttp.client.URL = ByteURL

    aiohttp.client_reqrep.ClientRequest = PatchedClientRequest
    aiohttp.client.ClientRequest = PatchedClientRequest
    aiohttp.connector.ClientRequest = PatchedClientRequest
    aiohttp.ClientRequest = PatchedClientRequest
    aiohttp.ClientSession.__init__.__kwdefaults__['request_class'] = \
        PatchedClientRequest

    aiohttp.http_writer._py_serialize_headers = _patched_serialize_headers
    aiohttp.http_writer._serialize_headers = _patched_serialize_headers
    aiohttp.http_writer._c_serialize_headers = _patched_serialize_headers

    urllib.parse._implicit_encoding = 'iso-8859-1'
    urllib.parse._decode_args.__defaults__ = ('iso-8859-1', 'strict')
    urllib.parse._encode_result.__defaults__ = ('iso-8859-1', 'strict')
