import argparse
import importlib

MODULES = [
    'httpget',
    'irc',
    'telnet',
    'http_lockout',
    'http_headers',
    'http_ports',
]


def create_argparser() -> argparse.ArgumentParser:

    parser = argparse.ArgumentParser(prog='probe')
    subparsers = parser.add_subparsers()

    for mname in MODULES:
        mod = importlib.import_module('.'+mname, __package__)

        # mypy doesn't know how to look into modules imported this way
        m_help = mod.__doc__
        m_probe = mod.probe  # type: ignore
        m_init = mod.init_subparser  # type: ignore

        subparser = subparsers.add_parser(mname.replace('_', '-'),
                                          help=m_help)
        subparser.set_defaults(func=m_probe)
        m_init(subparser)

    return parser


def main() -> None:

    parser = create_argparser()
    args = parser.parse_args()

    if not hasattr(args, 'func'):
        parser.error('No probe module was selected')

    args.func(args)


if __name__ == '__main__':
    main()
