"""Probe for censored keywords over Telnet."""

import argparse
import socket
import sys
import time

TELNET_DO_ECHO = b'''\xff\xfd\x01'''

PACKETS = [
    b'''\xff\xfb\x25\xff\xfd\x03\xff\xfb\x18\xff
\xfb\x1f\xff\xfb\x20\xff\xfb\x21\xff\xfb
\x22\xff\xfb\x27\xff\xfd\x05''',
    TELNET_DO_ECHO,
    '足球法轮多维盘古乐队无界花园网华夏文摘动态网延安日记博讯世界经济导报\n'.encode('gbk'),
]

BUF_SIZE = 1024


def recv_all(sock: socket.socket) -> None:
    while True:
        data = sock.recv(BUF_SIZE)
        print(data.decode('gbk', errors='ignore'))
        if len(data) < BUF_SIZE:
            break
    print('<< ALL RECEIVED >>')


def probe(args: argparse.Namespace) -> None:
    try:
        state = 'creating client socket'
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:

            state = f'connecting to {args.addr}:{args.port}'
            s.connect((args.addr, args.port))

            state = 'sending and receiving data'
            s.send(PACKETS[0])
            time.sleep(0.3)
            recv_all(s)

            s.send(PACKETS[1])
            time.sleep(0.3)
            recv_all(s)

            s.send(PACKETS[2])
            time.sleep(0.3)

            s.send(TELNET_DO_ECHO)
            time.sleep(0.3)
            recv_all(s)

    except Exception as e:
        sys.stderr.write(f'Error {state}: {e}\n')
        sys.exit(1)


def init_subparser(parser: argparse.ArgumentParser) -> None:
    parser.add_argument('-s', '--server', dest='addr',
                        help='Telnet server address')
    parser.add_argument('-p', '--port', dest='port',
                        type=int, default=23,
                        help='Telnet server port. Default 23')
