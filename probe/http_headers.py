"""Probe treatment of censored keywords in HTTP request headers."""

import argparse
import asyncio
import csv
import os
import random
import ssl
import time

from functools import lru_cache
from itertools import chain
from urllib.parse import SplitResult, urlsplit, quote as urlquote

from typing import (
    AsyncIterable, Dict, Iterable, List, NamedTuple, Optional
)

from .util import (
    CertCapturingClientResponse, ENCODINGS, MemoizedResolver,
    OutputFile, Reporter, TextIO, UnreliableServerError,
    file2lines, non_checking_client_ssl_context
)
from .patch_aiohttp import patch_aiohttp_for_byte_urls  # type: ignore

import aiohttp
import aiostream  # type: ignore
import idna  # type: ignore
import toml
from tqdm import tqdm  # type: ignore


# encoders for request headers (see variants_for_generic_header)
def _make_mimencode():
    from email.charset import Charset, BASE64, QP

    b64 = {}
    qp = {}
    for enc in ENCODINGS:
        C_b64 = Charset(enc)
        C_b64.header_encoding = BASE64
        b64[enc] = C_b64.header_encode

        C_qp = Charset(enc)
        C_qp.header_encoding = QP
        qp[enc] = C_qp.header_encode

    return {
        "b64": b64,
        "qp": qp
    }


mimencode = _make_mimencode()
del _make_mimencode


class Origin(NamedTuple):
    scheme: str
    hostname: str
    port: Optional[int]

    # @cached_property is only available in 3.8 and also doesn't work
    # with NamedTuples.  mypy doesn't support @property @lru_cache().
    @lru_cache(maxsize=1)
    def origin(self) -> str:
        if self.port:
            return f"{self.scheme}://{self.hostname}:{self.port}"
        else:
            return f"{self.scheme}://{self.hostname}"

    def __str__(self) -> str:
        return self.origin()


class Config:
    def __init__(self, args: argparse.Namespace, rep: Reporter):
        self.skip_sanity_check = args.skip_sanity_check

        configfile = args.config_file
        try:
            data = toml.load(configfile)
        except Exception as e:
            rep.fatal_exception(e, f"{configfile}: parse error")

        try:
            self.sites: Dict[Origin, SplitResult] = {}
            for site in data['sites']:
                s = urlsplit(site)
                key = Origin(s.scheme, s.hostname, s.port)
                if key in self.sites:
                    raise ValueError(f"extra URL for origin {key}")
                self.sites[key] = s

            self.blocked_url = data['expected-blocked']
            self.benign_url = data['expected-benign']
            self.test_template = data['test-template']
            self.post_template = data['post-template']
            self.abandon_after = data['abandon-after']
            self.interval = data['interval']
            self.successes = data['successes']
            self.timeout = data['timeout']

            self.invariant_headers = {}
            self.keyword_headers = []
            for k, v in data['request-headers'].items():
                if '{keyword}' in v or '{domain}' in v:
                    self.keyword_headers.append((k, v))
                else:
                    self.invariant_headers[k] = v.encode("ascii")

            self.rng = random.Random()

        except Exception as e:
            rep.fatal_exception(
                e, f"{configfile}: required field missing or invalid")

        try:
            self.blocked_keyword_list = file2lines(args.blocked_keyword_list)
        except Exception as e:
            rep.fatal_exception(
                e, f"{args.blocked_keyword_list}")

        # use typical SSL settings, but don't authenticate the server
        # (we will instead log the certificate we get).
        self.ssl_ctx = non_checking_client_ssl_context()

    def benign_string(self) -> str:
        """Generate a short random string that is extremely unlikely to be
           censored, and which can appear in an HTTP request line, a
           domain name component, or an arbitrary HTTP request header
           without needing any quotation."""

        LETTERS = "bcdfghjklmnprstv"
        return ''.join(self.rng.choice(LETTERS)
                       for _ in range(self.rng.randint(5, 9)))

    async def preresolve_sites(self, resolver: MemoizedResolver) -> None:
        good_sites = await resolver.resolve_many(
            key.hostname for key in self.sites.keys())
        # discard any sites whose names could not be resolved
        self.sites = {
            k: v for k, v in self.sites.items()
            if k.hostname in good_sites
        }


class TestRequest(NamedTuple):
    """All the information required to construct an experimental HTTP
       request."""
    keyword: str
    location: str
    encoding: str
    armor: str
    method: str
    path: bytes
    headers: Dict[str, bytes]
    body: bytes

    @classmethod
    def filler(cls, path: str) -> 'TestRequest':
        """Generate a TestRequest object for when we need to send an HTTP
           request that isn't actually part of the experiment.  Only
           supports simple GET requests with no additional headers.
        """
        return cls("", "", "", "", "GET", path.encode("ascii"), {}, b'')

    @classmethod
    def get(cls,
            keyword: str,
            location: str,
            encoding: str,
            armor: str,
            path: bytes,
            headers: Dict[str, bytes]) -> 'TestRequest':
        return cls(keyword, location, encoding, armor,
                   "GET", path, headers, b'')

    @classmethod
    def post(cls,
             keyword: str,
             location: str,
             encoding: str,
             armor: str,
             path: bytes,
             headers: Dict[str, bytes],
             content_type: str = 'application/x-www-form-urlencoded',
             body: bytes = b'') -> 'TestRequest':

        headers["Content-Type"] = content_type.encode("ascii")
        headers["Content-Length"] = str(len(body)).encode("ascii")
        return cls(keyword, location, encoding, armor,
                   "POST", path, headers, body)


def variants_for_request_line(keyword: str,
                              template: str,
                              headers: Dict[str, bytes]
                              ) -> Iterable[TestRequest]:

    dupes = set()
    for encoding in ENCODINGS:
        try:
            # One request with the keyword encoded in ENCODING
            # but not armored...
            path = template.format(keyword=keyword).encode(encoding)
            if path not in dupes:
                dupes.add(path)
                yield TestRequest.get(keyword, "path", encoding, "bare",
                                      path, headers.copy())

            # and a second request with the keyword encoded and then
            # URL-quoted.
            qkeyword = urlquote(keyword, encoding=encoding)
            if qkeyword != keyword:
                path = template.format(keyword=qkeyword).encode(encoding)
                if path not in dupes:
                    dupes.add(path)
                    yield TestRequest.get(keyword, "path", encoding,
                                          "url-quote", path, headers.copy())

        except UnicodeEncodeError:
            # This keyword cannot be represented in this encoding.  Skip it.
            pass


def variants_for_generic_header(keyword: str,
                                template: str,
                                header: str,
                                path: bytes,
                                other_headers: Dict[str, bytes]
                                ) -> Iterable[TestRequest]:
    if '{' in template:
        value = template.format(keyword=keyword)
    else:
        value = template

    dupes = set()
    for encoding in ENCODINGS:
        try:
            # One request with the value encoded in ENCODING but not armored...
            val = value.encode(encoding)
            if val not in dupes:
                dupes.add(val)
                headers = other_headers.copy()
                headers[header] = val
                yield TestRequest.get(keyword, header, encoding, "bare",
                                      path, headers)

            # two requests with RFC 2047 quoting...
            val = mimencode["b64"][encoding](value).encode("ascii")
            if val not in dupes:
                dupes.add(val)
                headers = other_headers.copy()
                headers[header] = val
                yield TestRequest.get(keyword, header, encoding, "base64",
                                      path, headers)

            val = mimencode["qp"][encoding](value).encode("ascii")
            if val not in dupes:
                dupes.add(val)
                headers = other_headers.copy()
                headers[header] = val
                yield TestRequest.get(keyword, header, encoding, "qp",
                                      path, headers)

            # and one request with (inappropriate) url-quoting.
            val = urlquote(value, encoding=encoding).encode("ascii")
            if val not in dupes:
                dupes.add(val)
                headers = other_headers.copy()
                headers[header] = val
                yield TestRequest.get(keyword, header, encoding, "url-quote",
                                      path, headers)

        except UnicodeEncodeError:
            # This keyword cannot be represented in this encoding.  Skip it.
            pass


def variants_for_host_header(keyword: str,
                             domain: str,
                             template: str,
                             path: bytes,
                             other_headers: Dict[str, bytes]
                             ) -> Iterable[TestRequest]:

    gtemplate = template.format(keyword="{keyword}", domain=domain)

    yield from variants_for_generic_header(keyword, gtemplate,
                                           "Host", path, other_headers)

    # properly idna-encoded (if possible)
    try:
        value = template.format(keyword=(keyword
                                         .replace('_', '-')
                                         .replace(' ', '-')
                                         .replace('.', '-')),
                                domain=domain)
        val = idna.encode(value)
        # IDNA coding is so different from the encoding of anything
        # generated by variants_for_generic_header that we don't have
        # to worry about duplicates, *except* when it returns the
        # value unchanged, in which case it's a duplicate of the
        # ascii/bare variant and we should skip it.
        if val.decode("ascii") != value:
            headers = other_headers.copy()
            headers["Host"] = val

            # Calling IDNA an armor of UTF-8 is technically incorrect but
            # convenient for downstream analysis.
            yield TestRequest.get(keyword, "Host", "utf-8", "idna",
                                  path, headers)

    except idna.IDNAError:
        pass


def variants_for_post_body(keyword: str,
                           path: bytes,
                           headers: Dict[str, bytes]
                           ) -> Iterable[TestRequest]:
    value = "k="+keyword
    dupes = set()
    for encoding in ENCODINGS:
        try:
            # One request with the keyword encoded in ENCODING
            # but not armored...
            val = value.encode(encoding)
            if val not in dupes:
                dupes.add(val)
                yield TestRequest.post(keyword, "body", encoding, "bare",
                                       path, headers.copy(), body=val)

            # two requests with RFC 2047 quoting...
            val = mimencode["b64"][encoding](value).encode("ascii")
            if val not in dupes:
                dupes.add(val)
                yield TestRequest.post(keyword, "body", encoding, "base64",
                                       path, headers.copy(), body=val)

            val = mimencode["qp"][encoding](value).encode("ascii")
            if val not in dupes:
                dupes.add(val)
                yield TestRequest.post(keyword, "body", encoding, "qp",
                                       path, headers.copy(), body=val)

            # and one request with (inappropriate) url-quoting.
            val = urlquote(value, encoding=encoding).encode("ascii")
            if val not in dupes:
                dupes.add(val)
                yield TestRequest.post(keyword, "body", encoding, "url-quote",
                                       path, headers.copy(), body=val)

        except UnicodeEncodeError:
            # This keyword cannot be represented in this encoding.  Skip it.
            pass


def tests_for_keyword(keyword: str, domain: str,
                      config: Config) -> Iterable[TestRequest]:

    url_template = config.test_template
    benign = config.benign_string()
    benign_headers = {
        k: v.format(keyword=benign, domain=domain).encode("ascii")
        for k, v in config.keyword_headers
    }

    # Keyword in the request line
    yield from variants_for_request_line(keyword, url_template, benign_headers)

    # Keyword in each of the keyword headers
    reqline = url_template.format(keyword=benign).encode("ascii")
    for k, v in config.keyword_headers:
        if k == 'Host':
            yield from variants_for_host_header(keyword, domain, v,
                                                reqline, benign_headers)
        else:
            yield from variants_for_generic_header(keyword, v, k,
                                                   reqline, benign_headers)

    # Keyword in the POST body
    reqline = config.post_template.format(keyword=benign).encode("ascii")
    yield from variants_for_post_body(keyword, reqline, benign_headers)


class TestResponse(NamedTuple):
    req: TestRequest
    site: Origin
    ok: bool
    cert: str  # '' for unencrypted requests or if not available


async def send_http_request(req: TestRequest,
                            site: Origin,
                            base_url: SplitResult,
                            session: aiohttp.ClientSession,
                            ssl_ctx: ssl.SSLContext,
                            rep: Reporter,
                            want_certificate: bool) -> TestResponse:

    def construct_netloc(host: str, port: Optional[int]) -> str:
        if port is None:
            return host
        else:
            return host + ':' + str(port)

    if req.location == "Host":
        # edit the base URL and insert the keyword into the hostname.
        # city.example.com becomes city-keyword.example.com.
        # our patched DNS resolver will treat this the same as
        # city.example.com but the keyword will go into the
        # SNI (cleartext) as well as the Host (encrypted).
        labels = (base_url.hostname or '').split('.')
        labels[0] = labels[0] + '-' + (req.keyword
                                       .replace('_', '-')
                                       .replace(' ', '-')
                                       .replace('.', '-'))
        netloc = construct_netloc('.'.join(labels), base_url.port)
        rep.debug(f"modifying {base_url.hostname}:{base_url.port} to {netloc}")
        url = SplitResult(
            scheme=base_url.scheme,
            netloc=netloc,
            path=base_url.path,
            query=base_url.query,
            fragment=base_url.fragment
        ).geturl().encode("utf-8") + req.path

    else:
        url = base_url.geturl().encode("ascii") + req.path

    xurl = url.decode('ascii', 'backslashreplace')
    try:
        cert = ''
        async with session.request(req.method,
                                   url,  # type: ignore  # monkeypatched
                                   headers=req.headers,
                                   data=req.body,
                                   ssl=ssl_ctx) as resp:

            if want_certificate and url[:5].lower() == b"https":
                assert isinstance(resp, CertCapturingClientResponse)
                cert = resp.server_certificate

            # consume and discard the full response
            await resp.read()

            # treat Method Not Allowed and Bad Request as "successful"
            # since we know they're coming from the real server in response
            # to our dodgy requests
            ok = resp.status in (200, 400, 405)

            rep.debug(f"{req.method} {xurl}: {resp.status} {resp.reason}")
            return TestResponse(req, site, ok, cert)

    except Exception as e:
        if isinstance(e, asyncio.TimeoutError):
            rep.debug(f"{req.method} {xurl}: timeout")
        else:
            rep.debug(f"{req.method} {xurl}: {e}")
            if not isinstance(e, OSError):
                rep.debug_exception(e)

        return TestResponse(req, site, False, '')


async def probe_lockout(site: Origin,
                        base_url: SplitResult,
                        session: aiohttp.ClientSession,
                        config: Config,
                        rep: Reporter) -> bool:

    limit = config.abandon_after
    interval = config.interval
    successes_required = config.successes
    req = TestRequest.filler(config.benign_url)
    ssl_ctx = config.ssl_ctx

    start = start_cycle = time.monotonic()
    elapsed = 0.
    successes = 0
    while elapsed < limit:
        resp = await send_http_request(req, site, base_url,
                                       session, ssl_ctx, rep,
                                       want_certificate=False)
        if resp.ok:
            successes += 1
        else:
            successes = 0

        if successes >= successes_required:
            return True

        pelapsed = time.monotonic() - start_cycle
        delay = interval - pelapsed
        if delay > 0:
            await asyncio.sleep(delay)

        start_cycle = time.monotonic()
        elapsed = start_cycle - start

    return False


async def probe_one_request(request: TestRequest,
                            site: Origin,
                            base_url: SplitResult,
                            session: aiohttp.ClientSession,
                            config: Config,
                            rep: Reporter) -> TestResponse:

    limit = config.abandon_after
    interval = config.interval
    successes_required = config.successes
    ssl_ctx = config.ssl_ctx

    start = start_cycle = time.monotonic()
    elapsed = 0.
    successes = 0
    failures = 0
    certificates = set()
    while elapsed < limit:
        resp = await send_http_request(request, site, base_url, session,
                                       ssl_ctx, rep,
                                       want_certificate=True)
        certificates.add(resp.cert)
        if resp.ok:
            successes += 1
            failures = 0
        else:
            successes = 0
            failures += 1

        if successes >= successes_required or failures >= successes_required:
            ok = (successes >= successes_required)
            return TestResponse(request, site, ok,
                                '|'.join(sorted(certificates)))

        pelapsed = time.monotonic() - start_cycle
        delay = interval - pelapsed
        if delay > 0:
            await asyncio.sleep(delay)

        start_cycle = time.monotonic()
        elapsed = start_cycle - start

    raise UnreliableServerError


async def probe_one_site(site: Origin,
                         requests: List[TestRequest],
                         session: aiohttp.ClientSession,
                         config: Config,
                         rep: Reporter) -> AsyncIterable[TestResponse]:

    base_url = config.sites[site]

    with tqdm(iterable=requests, unit="probe") as bar:

        # first, make sure we're not locked out already
        bar.set_description(f"{site}: testing availability")
        bar.refresh()
        if not (await probe_lockout(site, base_url, session, config, rep)):
            rep.warning(f"abandoning {site}: down or locked out already")
            return

        # if desired, now send a request that we know ought to fail
        if not config.skip_sanity_check:
            bar.set_description(f"{site}: sanity check")
            bar.refresh()
            req = TestRequest.filler(config.blocked_url)
            resp = await send_http_request(req, site, base_url,
                                           session, config.ssl_ctx, rep,
                                           want_certificate=False)
            if resp.ok:
                rep.warning(f"abandoning {site}: "
                            "expected blocked was not blocked")
                return

            # finally, wait out the lockout that that may have triggered
            if not (await probe_lockout(site, base_url, session, config, rep)):
                rep.warning(f"abandoning {site}: could not escape lockout")
                return

        bar.set_description(f"{site}: probing")
        bar.set_postfix(filtered=0)
        bar.refresh()
        filtered = 0
        for i, req in enumerate(bar):
            try:
                status = await probe_one_request(req, site, base_url,
                                                 session, config, rep)
            except UnreliableServerError:
                rep.warning(f"abandoning {site}: responses unreliable")
                return

            yield status
            if not status.ok:
                filtered += 1
                bar.set_postfix(filtered=filtered)
                bar.set_description(f"{site}: lockout")
                bar.refresh()
                if not (await probe_lockout(site, base_url, session,
                                            config, rep)):
                    rep.warning(
                        f"abandoning {site}: could not escape lockout"
                        f" after {filtered} filtered queries")
                    break

                bar.set_description(f"{site}: probing")
                bar.refresh()


async def probe_all_sites(outf: TextIO,
                          requests: List[TestRequest],
                          config: Config,
                          rep: Reporter) -> None:

    resolver = MemoizedResolver(aiohttp.resolver.DefaultResolver(), rep)
    try:
        await config.preresolve_sites(resolver)
        if not config.sites:
            rep.fatal("No usable servers, exiting.")

        async with aiohttp.ClientSession(
                timeout=aiohttp.ClientTimeout(total=config.timeout),
                headers=config.invariant_headers,
                cookie_jar=aiohttp.DummyCookieJar(),
                connector=aiohttp.TCPConnector(resolver=resolver,
                                               use_dns_cache=False),
                response_class=CertCapturingClientResponse
        ) as session:

            wr = csv.DictWriter(outf, fieldnames=[
                'keyword', 'location', 'encoding', 'armor',
                'site', 'ok', 'certs'
            ], dialect='unix', quoting=csv.QUOTE_MINIMAL)

            wr.writeheader()

            async with aiostream.stream.merge(*[
                    probe_one_site(site, requests, session, config, rep)
                    for site in config.sites.keys()
            ]).stream() as responses:
                async for resp in responses:
                    req = resp.req
                    wr.writerow({
                        'keyword': req.keyword,
                        'location': req.location,
                        'encoding': req.encoding,
                        'armor': req.armor,
                        'site': resp.site.origin(),
                        'ok': resp.ok,
                        'certs': resp.cert
                    })

    finally:
        await resolver.close()


def probe(args: argparse.Namespace) -> None:

    patch_aiohttp_for_byte_urls()

    with Reporter(args) as rep:
        config = Config(args, rep)
        rep.exit_if_errors()

        try:
            outfile = args.out_file
            outpartial = outfile + ".partial"
            with OutputFile(outpartial, "xt") as outf:
                # mypy can't figure out that this usage of OutputFile will
                # always produce a TextIO, give it a hint
                assert isinstance(outf, TextIO)

                tests = list(chain.from_iterable(
                    tests_for_keyword(keyword, args.domain, config)
                    for keyword in config.blocked_keyword_list
                ))

                loop = asyncio.get_event_loop()
                try:
                    loop.run_until_complete(
                        probe_all_sites(outf, tests, config, rep))
                finally:
                    loop.run_until_complete(loop.shutdown_asyncgens())
                    loop.close()

            rep.exit_if_errors()
            os.rename(outpartial, outfile)

        except Exception as e:
            rep.fatal_exception(e)


def init_subparser(parser: argparse.ArgumentParser) -> None:
    parser.add_argument('config_file', help='URL configuration file')
    parser.add_argument('blocked_keyword_list',
                        help='File containing a list of blocked keywords,'
                        ' one per line')
    parser.add_argument('out_file', help='File to write results to')
    parser.add_argument('-d', '--domain',
                        default='example.com',
                        help='Fake domain to use in Host headers')
    parser.add_argument('--skip-sanity-check', action='store_true',
                        help='Do not check whether sites experience any'
                        ' filtering at all before running the main test.')

    Reporter.add_cli_arguments(parser)
