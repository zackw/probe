#! /usr/bin/python3

"""Attempt to identify the writing system (Unicode "script") and
language of each "term" (word or short phrase) in the input.

Input should be one term per line, in UTF-8.  Output is in
CSV format, three columns:

    script,language,term

In each row, "term" will be the original term after NFKC
normalization, "script" will be one or more Unicode Script property
names (space-separated), and "language" will be one or more
ISO 639-3:2007 language codes (also space-separated).

If no command-line arguments are provided, reads keywords (one per
line) from stdin and prints CSV-format classification results to
stdout.  Otherwise, each argument is a file to process, and results
will be written to a file named "<original-basename>-wlc.csv".

"""

import argparse
import csv
import os.path
import sys
import unicodedata

import nltk
from nltk.classify.textcat import TextCat
import uniscripts

# plain strip(), with no arguments, doesn't remove enough junk from the
# ends of each line. this computes a superset including all the characters
# we want gone: everything in these categories...
JUNK = "".join(
    c
    for i in range(sys.maxunicode)
    for c in chr(i)
    if unicodedata.category(c) in ('Cc', 'Zl', 'Zp', 'Zs')
)
JUNK += (
    # ... plus *some* of the characters in Cf ...
    "\u00AD" # SOFT HYPHEN
    "\u200B" # ZERO WIDTH SPACE
    "\u200C" # ZERO WIDTH NON-JOINER
    "\u200D" # ZERO WIDTH JOINER
    "\u200E" # LEFT-TO-RIGHT MARK
    "\u200F" # RIGHT-TO-LEFT MARK
    "\u202A" # LEFT-TO-RIGHT EMBEDDING
    "\u202B" # RIGHT-TO-LEFT EMBEDDING
    "\u202C" # POP DIRECTIONAL FORMATTING
    "\u202D" # LEFT-TO-RIGHT OVERRIDE
    "\u202E" # RIGHT-TO-LEFT OVERRIDE
    "\u2060" # WORD JOINER
    "\uFEFF" # ZERO WIDTH NO-BREAK SPACE

    # ... and the noncharacters (a subset of Cn).
    "\uFDD0\uFDD1\uFDD2\uFDD3\uFDD4\uFDD5\uFDD6\uFDD7"
    "\uFDD8\uFDD9\uFDDA\uFDDB\uFDDC\uFDDD\uFDDE\uFDDF"
    "\uFFFE\uFFFF"
    "\U0001FFFE\U0001FFFF"
    "\U0002FFFE\U0002FFFF"
    "\U0003FFFE\U0003FFFF"
    "\U0004FFFE\U0004FFFF"
    "\U0005FFFE\U0005FFFF"
    "\U0006FFFE\U0006FFFF"
    "\U0007FFFE\U0007FFFF"
    "\U0008FFFE\U0008FFFF"
    "\U0009FFFE\U0009FFFF"
    "\U000AFFFE\U000AFFFF"
    "\U000BFFFE\U000BFFFF"
    "\U000CFFFE\U000CFFFF"
    "\U000DFFFE\U000DFFFF"
    "\U000EFFFE\U000EFFFF"
    "\U000FFFFE\U000FFFFF"
    "\U0010FFFE\U0010FFFF"
)


def guess_scripts(term):
    scripts = set()
    for c in term:
        scripts.add(uniscripts.which_scripts(c)[0])

    scripts.discard("Inherited")
    scripts.discard("Common")
    scripts.discard("Unknown")

    if not scripts:
        return set(("Unknown",))
    else:
        return scripts


def process(ofp, ifp, tc):
    wr = csv.writer(ofp, dialect="unix", quoting=csv.QUOTE_MINIMAL)
    wr.writerow(("script", "language", "term"))

    for line in ifp:
        line = line.strip(JUNK)
        line = unicodedata.normalize("NFKD", line)
        line = line.strip(JUNK)
        if not line:
            continue

        # Special case: if the line is now entirely representable in
        # ASCII, call it "Latin,eng" and skip the rest.  This is
        # mainly so we don't have to have a separate category for
        # terms consisting entirely of digits and punctuation
        # (e.g. "8|9|6|4", which IIUC is a coded reference to
        # the Tienanmen Square protests).
        try:
            line.encode("ascii")
            wr.writerow(("Latin", "eng", line))
            continue
        except UnicodeEncodeError:
            pass

        scripts = guess_scripts(line)
        line = unicodedata.normalize("NFC", line)
        lang = tc.guess_language(line)
        if lang == "abk" and "Cyrillic" not in scripts:
            # I don't know why, but tc.guess_language says Abkhazian
            # whenever it doesn't actually know.  Assume it's wrong
            # unless the script is Cyrillic.  (Abkhazian *can* be
            # written using Latin or Georgian script, but that's
            # unlikely to happen for modern writing.)
            lang = "und" # 'the language in the data has not been identified'

        if "Han" in scripts:
            # Unicode doesn't actually distinguish between simplified
            # and traditional characters, so for now we're falling back
            # to the approximation of "if it can be encoded in GB 2312
            # it's simplified".  (Note: GB 18030 can represent all of
            # Unicode, so it's useless to us.)
            scripts.remove("Han")

            try:
                line.encode("gb2312")
                scripts.add("Han-s")

            except UnicodeEncodeError:
                scripts.add("Han-t")

        wr.writerow((" ".join(sorted(scripts)), lang, line))


def main():
    ap = argparse.ArgumentParser(description=__doc__)
    ap.add_argument("files", nargs="*", metavar="file",
                    help="File listing terms to be classified.")
    args = ap.parse_args()

    # data required by TextCat
    nltk.download('crubadan')
    nltk.download('punkt')
    tc = TextCat()

    if args.files:
        for infname in args.files:
            oufname = os.path.splitext(infname)[0] + "-wlc.csv"
            with open(infname, "rt", encoding="utf-8") as ifp, \
                 open(oufname, "wt", encoding="utf-8") as ofp:

                process(ofp, ifp, tc)

    else:
        with sys.stdin as ifp, sys.stdout as ofp:
            process(ofp, ifp, tc)

main()
