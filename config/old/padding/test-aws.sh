mkdir -p log/padding

python -m probe httpget -m 1 -c padding/aws.gn.p0.toml -o log/padding/aws.gn.p0.log -k gfw.chs.txt
python -m probe httpget -m 1 -c padding/aws.gn.p500.toml -o log/padding/aws.gn.p500.log -k gfw.chs.txt
python -m probe httpget -m 1 -c padding/aws.gn.p1000.toml -o log/padding/aws.gn.p1000.log -k gfw.chs.txt
python -m probe httpget -m 1 -c padding/aws.gn.p1500.toml -o log/padding/aws.gn.p1500.log -k gfw.chs.txt
python -m probe httpget -m 1 -c padding/aws.gn.p2000.toml -o log/padding/aws.gn.p2000.log -k gfw.chs.txt
python -m probe httpget -m 1 -c padding/aws.gn.p2500.toml -o log/padding/aws.gn.p2500.log -k gfw.chs.txt
