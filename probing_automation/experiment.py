import sys
import os
import time
from ansible.parsing.dataloader import DataLoader
from ansible.inventory.manager import InventoryManager


def get_inventory_servers():
    inventory_file_name = 'server_inventory.cfg'
    data_loader = DataLoader()
    inventory = InventoryManager(loader = data_loader, sources=[inventory_file_name])

    inventory.parse_sources()
    
    server_list = []
    for host in inventory.get_hosts():
        server_list.append(host.vars["node_name"])

    return server_list


def run_experiment(experiment_type, wordlist):
    if(experiment_type == "http"):
        run_http_experiment(wordlist)
    elif(experiment_type == "https_ca_signed"):
        run_https_ca_signed_experiment(wordlist)
    elif(experiment_type == "https_self_signed"):
        run_https_self_signed_experiment(wordlist)


def run_http_experiment(wordlist):
    server_list = get_inventory_servers()

    os.system("mkdir -p experiment_logs")

    for server in server_list:
        time.sleep(1)
        os.system("screen -S http_%s_%s -L -d -m bash -c \"./probe.sh httpget -m all -c config/http_configs/%s.toml -o experiment_logs/http_%s_%s.log -k wordlists/%s_list.txt --ignore-sanity\""%(wordlist,server,server,wordlist,server,wordlist))

def run_https_ca_signed_experiment(wordlist):
    server_list = get_inventory_servers()

    os.system("mkdir -p experiment_logs")

    for server in server_list:
        time.sleep(1)
        os.system("screen -S https-ca-signed_%s_%s -L -d -m bash -c \"./probe.sh httpget -m all -c config/https-ca-signed_configs/%s.toml -o experiment_logs/https-ca-signed_%s_%s.log -k wordlists/%s_list.txt --ignore-sanity\""%(wordlist,server,server,wordlist,server,wordlist))

def run_https_self_signed_experiment(wordlist):
    server_list = get_inventory_servers()

    os.system("mkdir -p experiment_logs")
    
    for server in server_list:
        time.sleep(1)
        os.system("screen -S https-self-signed_%s_%s -L -d -m bash -c \"./probe.sh httpget -m all -c config/https-self-signed_configs/%s.toml -o experiment_logs/https-self-signed_%s_%s.log -k wordlists/%s_list.txt --ignore-sanity\""%(wordlist,server,server,wordlist,server,wordlist))


if __name__ == '__main__':
    experiment_type = sys.argv[1]
    wordlist = sys.argv[2]

    run_experiment(experiment_type, wordlist)