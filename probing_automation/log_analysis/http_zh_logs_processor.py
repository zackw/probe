import csv
import random
import os
import sys

from py_stringmatching import OverlapCoefficient
from prettytable import PrettyTable
import time

def analyze_keyword_server_data(log_folder, wordlist, translate):
    
    #Create analysis log file
    if not os.path.exists("results/http_zh_results/%s"%os.path.basename(log_folder)):
        os.makedirs("results/http_zh_results/%s"%os.path.basename(log_folder))
    results_file = open("results/http_zh_results/%s/results_%s_%s.txt"%(os.path.basename(log_folder),wordlist,os.path.basename(log_folder)), 'w') 
    table_file = open("results/http_zh_results/%s/table_%s_%s.txt"%(os.path.basename(log_folder), wordlist, os.path.basename(log_folder)), 'w') 

    client_blocked_keywords = {}
    
    ################################################################################################
    #Build table fields
    # We have to cycle through the existing log files first to understand which data is available
    table = PrettyTable()
    table.title = "%s_%s"%(wordlist, os.path.basename(log_folder))
    
    longest_dict = {}
    for client_folder in os.listdir(log_folder):
        if(client_folder == ".DS_Store"):
            continue
        
        keyword_server_dict = {}

        for log_file in os.listdir(log_folder + "/" + client_folder + "/experiment_logs/"):
            if(log_file == ".DS_Store" or wordlist not in log_file):
                continue
            
            keyword_server = log_file.split("_")[2][:-4]
            keyword_server_dict[keyword_server] = "dummy"

        if(len(keyword_server_dict.keys()) > len(longest_dict.keys())):
            longest_dict = keyword_server_dict

    field_names = ["Client/Server"]
    for client in sorted(longest_dict.keys()):
        field_names.append(client)
    table.field_names = field_names
    ################################################################################################


    #Gather statistics about each client
    for client_folder in os.listdir(log_folder):
        if(client_folder == ".DS_Store"):
            continue
        

        blocked_keywords_search = set()
        blocked_keywords_update = set()
        keyword_server_dict = {}
        
        print("[*] Inspecting client folder: %s, wordlist: %s"%(client_folder, wordlist))

        #Inspect results for each keyword server tested by the client
        for log_file in os.listdir(log_folder + "/" + client_folder + "/experiment_logs/"):
            if(log_file == ".DS_Store" or wordlist not in log_file):
                continue


            log_path = log_folder + "/" + client_folder + "/experiment_logs/"+log_file
            print(log_path)

            keyword_server = log_file.split("_")[2][:-4]
            keyword_server_dict[keyword_server] = [set(), set()]

            log_data = open(log_path, 'r')
            csv_reader_full = csv.reader(log_data, delimiter=',')
            for n, row in enumerate(csv_reader_full):
                if(n == 0):
                    continue

                if(row[2] == "True"):            
                    if("search?k=" in row[3]):
                        blocked_keywords_search.add(row[0])
                        keyword_server_dict[keyword_server][0].add(row[0])
                    elif("update?k=" in row[3]):
                        blocked_keywords_update.add(row[0])
                        keyword_server_dict[keyword_server][1].add(row[0])

                
        
        #Compute overlaps between keywords blocked with/without search?k=
        blocked_keywords = blocked_keywords_search | blocked_keywords_update
        blocked_keywords_search_only = blocked_keywords_search - blocked_keywords_update
        blocked_keywords_update_only = blocked_keywords_update - blocked_keywords_search

        #Keep track of blocked keywords by client
        client_blocked_keywords[client_folder] = blocked_keywords

        #Generate logs
        #results_file = sys.stdout

        print("--------------------------------",file = results_file)
        print("Client: %s"%(client_folder),file = results_file)
        print("--------------------------------",file = results_file)
        print("Number of blocked keywords: %d"%len(blocked_keywords),file = results_file)
        print("Number of blocked keywords (search-only): %d"%(len(blocked_keywords_search_only)),file = results_file)
        print("Number of blocked keywords (update-only): %d"%(len(blocked_keywords_update_only)),file = results_file)
        print("",file = results_file)


        print("# Individual server testing:",file = results_file)

        table_row = [client_folder.split("-")[1]]
        stride_correction = 1
        for n, key in enumerate(sorted(keyword_server_dict.keys())):
            target_blocked_keywords = keyword_server_dict[key][0] | keyword_server_dict[key][1]
            target_blocked_keywords_search_only = keyword_server_dict[key][0] - keyword_server_dict[key][1]
            target_blocked_keywords_update_only = keyword_server_dict[key][1] - keyword_server_dict[key][0]
            target_blocked_keywords_search_and_update = keyword_server_dict[key][0] & keyword_server_dict[key][1]

            print("* Target server: %s"%key,file = results_file)
            print("Number of blocked keywords: %d"%len(target_blocked_keywords),file = results_file)
            print("Number of blocked keywords (search-only): %d"%(len(target_blocked_keywords_search_only)),file = results_file)
            print("Number of blocked keywords (update-only): %d"%(len(target_blocked_keywords_update_only)),file = results_file)
            print("Number of blocked keywords (both search & update): %d"%(len(target_blocked_keywords_search_and_update)),file = results_file)
            print("",file = results_file)

            try:
                while(field_names[n+stride_correction] != key):
                    if(client_folder.split("-")[1] == field_names[n+stride_correction]):
                        table_row.append("-")
                    else:
                        table_row.append("N/A")
                    stride_correction += 1
                if(len(target_blocked_keywords) == 0):
                    table_row.append("-")
                else:
                    table_row.append("%d / %d"%(len(target_blocked_keywords_search_only),len(target_blocked_keywords)))
            except Exception:
                while(len(table_row) < len(table.field_names)):
                    table_row.append("N/A")
                #break

        #print(table_row)
        #print(table.field_names)
        table.add_row(table_row)


        
        print("* Blocked keywords when using search?k={keyword}:",file = results_file)
        print(blocked_keywords_search_only,file = results_file)

        if(translate == "y"):
            translations = []
            for w in blocked_keywords:
                try:
                    translator = Translator()
                    translation = translator.translate(w.strip(), dest="en")
                    translations.append(translation.text)
                except Exception as e:
                    print(e)
                    print("Unable to translate %s. Skipping..."%w)
                    translations.append(w)


            print("* Translations:", file= results_file)
            print(translations,file = results_file)
            print("\n",file = results_file)
            

        print("# Keyword dump:",file = results_file)
        print("* All blocked keywords:",file = results_file)
        print(blocked_keywords,file = results_file)
        print("\n",file = results_file)

    print(table, file=table_file)
    table_file.close()
    results_file.close()

    
    ############################
    #Compute Overlap Coefficient
    # https://medium.com/rapids-ai/similarity-in-graphs-jaccard-versus-the-overlap-coefficient-610e083b877d
    overlap_file = open("results/http_zh_results/%s/overlap_%s_%s.txt"%(os.path.basename(log_folder),wordlist,os.path.basename(log_folder)), 'w') 
    overlap_csv_file = open("results/http_zh_results/%s/overlap_%s_%s.csv"%(os.path.basename(log_folder),wordlist,os.path.basename(log_folder)), 'w') 

    print("Overlap Coefficient",file = overlap_file)
    print("------------------",file = overlap_file)

    print("Client, Client2, Overlap, Plus, Minus, TotalBlocked",file = overlap_csv_file)

    for client in client_blocked_keywords.keys():

        print("Client: %s, %d blocked keywords"%(client, len(client_blocked_keywords[client])),file = overlap_file)

        for client2 in client_blocked_keywords.keys():
            if(client == client2):
                continue

            oc = OverlapCoefficient()
            overlap_coeff = oc.get_raw_score(client_blocked_keywords[client], client_blocked_keywords[client2])
            A_minus_B = client_blocked_keywords[client] - client_blocked_keywords[client2]
            B_minus_A = client_blocked_keywords[client2] - client_blocked_keywords[client]
            print("%s - oc: %f, +%d -%d"%(client2,overlap_coeff, len(A_minus_B), len(B_minus_A)),file = overlap_file)
            print("%s,%s,%.2f,%s,%s,%s"%(client[5:], client2[5:], overlap_coeff, len(A_minus_B), len(B_minus_A), len(client_blocked_keywords[client])),file = overlap_csv_file)
        print("",file = overlap_file)

    overlap_file.close()
    

    



def process_http_zh_server_logs(translate):
    
    url_log_folders = "../trad_chinese_experiment_results/" 

    for timestamp_folder in os.listdir(url_log_folders):
        if(timestamp_folder == ".DS_Store"):
            continue
        
        print("########################################")
        print("Processing %s"%(url_log_folders + timestamp_folder))
        print("########################################")
        analyze_keyword_server_data(url_log_folders + timestamp_folder, "trad", translate)



    
    

