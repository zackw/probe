import csv
import random
import os
import sys
import argparse

import plotly.graph_objects as go 
from echo_logs_processor import process_echo_server_logs
from http_logs_processor import process_http_server_logs
from http_zh_logs_processor import process_http_zh_server_logs
from minimization_logs_processor import process_keyword_minimization_logs

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument('-u', '--http',
                    default="n",
                    help='Whether to produce http servers experiment results (y/n).')
    parser.add_argument('-z', '--trad_zh',
                    default="n",
                    help='Whether to produce http traditional chinese experiment results (y/n).')
    parser.add_argument('-m', '--minimization',
                    default="n",
                    help='Whether to produce keyword minimization experiment results (y/n).')
    parser.add_argument('-e', '--echo',
                    default="n",
                    help='Whether to produce echo servers experiment results (y/n).')
    parser.add_argument('-t', '--translate',
                    default="n",
                    help='Whether to translate blocked keywords (y/n).')

    args = parser.parse_args()

    if(args.http == "y"):
        process_http_server_logs(args.translate)
    
    if(args.echo == "y"):
        process_echo_server_logs(args.translate)

    if(args.trad_zh == "y"):
        process_http_zh_server_logs(args.translate)

    if(args.minimization == "y"):
        process_keyword_minimization_logs(args.translate)


    
