import csv
import random
import os
import sys

from googletrans import Translator

def analyze_echo_server_data(log_folder, translate):

    if not os.path.exists("results/echo_results"):
        os.makedirs("results/echo_results")
    results_file = open("results/echo_results/" + "results_%s.txt"%(os.path.basename(log_folder)), 'w') 


    client_blocked_keywords = {}

    for log_file in os.listdir(log_folder):
        if(log_file == ".DS_Store"):
            continue
        
        print("[*] Processing %s..."%log_file)
        log_path = log_folder+"/"+log_file

        log_data = open(log_path, 'r')
        csv_reader_full = csv.reader(log_data, delimiter=',')

        blocked_keywords= set()


        target_server_blocked_keywords = {
            "realmotor.jp": set(),
            "pegasus-idc.com": set(),
            "amazon.cn": set(),
        }
        

        for n, row in enumerate(csv_reader_full):
            if(n == 0):
                continue

            if(row[1] == "True"):            
                blocked_keywords.add(row[0])

                if("realmotor.jp" in row[2]):
                    target_server_blocked_keywords["realmotor.jp"].add(row[0])
                elif("pegasus-idc.com" in row[2]):
                    target_server_blocked_keywords["pegasus-idc.com"].add(row[0])
                elif("amazon.cn" in row[2]):
                    target_server_blocked_keywords["amazon.cn"].add(row[0])


        #Keep track of blocked keywords by client
        client_blocked_keywords[log_file.split("_")[0]] = blocked_keywords

        #Generate logs
        #results_file = sys.stdout

        print("--------------------------------",file = results_file)
        print("Log File: %s"%(log_path),file = results_file)
        print("--------------------------------",file = results_file)
        print("Number of blocked keywords: %d"%len(blocked_keywords),file = results_file)
        print("",file = results_file)


        print("# Individual server testing:",file = results_file)
        for key,value in target_server_blocked_keywords.items():
            target_blocked_keywords = value

            print("* Target server: %s"%key,file = results_file)
            print("Number of blocked keywords: %d"%len(target_blocked_keywords),file = results_file)
            print("",file = results_file)


        if(translate == "y"):
            translations = []
            for w in blocked_keywords:
                try:
                    translator = Translator()
                    translation = translator.translate(w.strip(), dest="en")
                    translations.append(translation.text)
                except Exception as e:
                    print(e)
                    print("Unable to translate %s. Skipping..."%w)
                    translations.append(w)


            print("* Translations:", file= results_file)
            print(translations,file = results_file)
            print("\n",file = results_file)
        
        print("# Keyword dump:",file = results_file)
        print("* All blocked keywords:",file = results_file)
        print(blocked_keywords,file = results_file)
        print("\n",file = results_file)

    results_file.close()



def process_echo_server_logs(translate):

    #Gather echo log folders
    echo_server_log_folders = [] 
    for folder in os.listdir("echo_logs"):
        if(folder != ".DS_Store"):
            echo_server_log_folders.append("echo_logs/" + folder)

    for log_folder in echo_server_log_folders:
        print("########################################")
        print("Processing %s"%log_folder)
        print("########################################")
        analyze_echo_server_data(log_folder, translate)