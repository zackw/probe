import plotly.graph_objects as go
import plotly.figure_factory as ff


def overlapHeatmap(clients, servers, values, data_type):
    """fig = go.Figure(data=go.Heatmap(
                   z=values,
                   x=servers,
                   y=clients,
                   hoverongaps = False,
                   colorscale = 'YlGnBu'))
    
    fig.show()"""
    #'YlGnBu'
    fig = ff.create_annotated_heatmap(x= servers, y=clients, z=values, showscale = True, colorscale = "YlGnBu_r")

    fig.update_layout(
        xaxis_title="Clients",
        yaxis_title="Clients",
        font=dict(
            family="Helvetica, sans-serif",
            size=16,
            color="black"
        )
    )

    fig.write_image("oc_%s.pdf"%data_type)

    #fig.show()


if __name__ == '__main__':


    clients_obfs = [
    "Hong Kong (1)", 
    "Hong Kong (2)", 
    "Shanghai", 
    "Beijing (1)", 
    "Beijing (2)", 
    "Guangzhou", 
    "[redacted], PA",
    ]


    overlapCoeffsWikipedia = [
        [1, 1, 0.757, 0.757, 0.757, 0.250], #hk1.okoze.net
        [1, 1, 1, 1, 1, 0.250], #hk2.okoze.net
        [0.757, 1, 1, 0.722, 0.722, 1], #shanghai.okoze.net
        [0.757, 1, 0.722, 1, 1, 0.750], #bj.spectrix.xyz
        [0.757, 1, 0.722, 1, 1, 0.750], #gz.spectrix.xyz
        [0.250, 0.250, 1, 0.750, 0.750, 1] #kenaz.andrew.cmu.edu
    ]

    overlapCoeffsChat = [
        [0,	0.99,	0.99,	1,	1,	1, 0.97], #Hong Kong (1)
        [0.99,	0, 0.93,	1,	1,	1, 0.91], #Hong Kong (2)
        [0.99,	0.93, 0,	1,	1,	1, 0.89], #Shanghai
        [1,	1,	1,	0,	1,	1, 0.98], #Beijing (1)
        [1,	1,	1,	1,	0,	1, 0.98], #Beijing (2)
        [1,	1,	1,	1,	1,	0, 0.98], #Guangzhou
        [0.97,	0.91,	0.89,	0.98,	0.98,	0.98, 0] #[redacted], PA
    ]

    overlapCoeffsXiaChu= [
        [1,	0.423,	1,	0.982,	0.990,	0], #hk1.okoze.net
        [0.423,	1,	1,	1,	1,  1], #hk2.okoze.net
        [1,	1,  1,	0.989,	0.993,	1], #shanghai.okoze.net
        [0.982,	1,	0.989,	1,	0.993,	1], #bj.spectrix.xyz
        [0.990,	1,	0.993,	0.993, 1, 1], #gz.spectrix.xyz
        [0,	1,	1,	1,	1,	1] #kenaz.andrew.cmu.edu
    ]


    #overlapHeatmap(list(reversed(clients_obfs)), clients_obfs, list(reversed(overlapCoeffsWikipedia)), "wikipedia")

    overlapHeatmap(list(reversed(clients_obfs)), clients_obfs, list(reversed(overlapCoeffsChat)), "chat")

    #overlapHeatmap(list(reversed(clients_obfs)), clients_obfs, list(reversed(overlapCoeffsXiaChu)), "xia_chu")