#!/usr/bin/env python
import subprocess
import socket
import os
import math
import csv
import numpy as np
import sys

import matplotlib
if os.environ.get('DISPLAY','') == '':
    print('no display found. Using non-interactive Agg backend')
    matplotlib.use('Agg')

import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
from matplotlib.ticker import MultipleLocator, FormatStrFormatter

#plt.rcParams['font.family'] = 'sans-serif'
#plt.rcParams['font.sans-serif'] = ['Helvetica']
plt.rcParams['font.family'] = 'sans-serif'
plt.rcParams['font.serif'] = 'Linux Biolinum'

colors = ["#67a9cf", "#ef8a62", "#0571b0", "#ca0020"]


minimized_wiki_keywords = ('八九', '色情', '新唐人', '法轮')

minimized_chat_keywords = ('请愿书', '代理', '学联', '色情', '学运', 'renminbao', '中国论坛', '九评共产党', '正见网', '藏独', '人民报',
    '轮大法', '动态网', '异见人士', '同修', '纪元', '无界', '异议人士', '多维', '十五周年', '反社会', '博讯', '学潮', '北京当局', '民运', '邪恶', 
    '民联', '新闻封锁', '看中国', '太子党', '法轮', '洗脑', '1989', '酷刑', 'vpn', '审江', '上海帮', '马三家', '历史的震撼', '京之春', '翻墙', '八九', 
    '明慧', '美国之音', '丁子霖', '六四', '新唐人', '记忆的呼唤', '诉江', '自由门', '天安门', '放光明', '赵紫阳', '天安门母亲', '盘古乐队', '鲍彤', '韩东方', 
    '高自联', '王丹', '魏京生', '血腥镇压', '迫害', '刘晓波', '法会', '真善忍', '封从德', '雪山狮子', '地下教会')

def ComputeDifferencesOverTime(wordlist, language):

    clients = [
        "hk1",
        "hk2",
        "shanghai",
        "beijing-okoze",
        "beijing-2",
        "guangzhou",
        "pittsburgh"
    ]

    client_differences = {}

    first_week_added = []
    second_week_added = []
    third_week_added = []
    fourth_week_added = []

    first_week_absolute_set = set()
    second_week_absolute_set = set()
    third_week_absolute_set = set()
    fourth_week_absolute_set = set()

    first_week_absolute_set_removed = set()
    second_week_absolute_set_removed = set()
    third_week_absolute_set_removed = set()
    fourth_week_absolute_set_removed = set()


    for client in clients:
        client_differences[client] = []

        first_set = set()
        second_set = set()
        third_set = set()
        fourth_set = set()

        try:
            first = open('../results/http_results/ts-2020-09-11/blocked_keywords/blocked_%s_logs-%s_%s.txt'%(wordlist,client,language), 'r')
            first_lines = first.readlines()
            for word in first_lines:
                first_set.add(word)

        except Exception as e:
            print(e)
            pass

        try:
            second = open('../results/http_results/ts-2020-09-18/blocked_keywords/blocked_%s_logs-%s_%s.txt'%(wordlist,client,language), 'r')
            second_lines = second.readlines()
            for word in second_lines:
                second_set.add(word)
        except:
            pass

        try:
            third = open('../results/http_results/ts-2020-09-28/blocked_keywords/blocked_%s_logs-%s_%s.txt'%(wordlist,client,language), 'r')
            third_lines = third.readlines()
            for word in third_lines:
                third_set.add(word)
        except:
            pass

        try:
            fourth = open('../results/http_results/ts-2020-10-15/blocked_keywords/blocked_%s_logs-%s_%s.txt'%(wordlist,client,language), 'r')
            fourth_lines = fourth.readlines()
            for word in fourth_lines:
                fourth_set.add(word)
        except:
            pass
        
        print(client)
        print(len(first_set))
        print(len(second_set))
        print(len(third_set))
        print(len(fourth_set))

        #In the first set, we just have the first added keywords
        client_differences[client].append([len(first_set),0])
        
        if(client != "guangzhou" and client != "beijing-2"):
            first_week_added.append(len(first_set))
            for w in first_set:
                first_week_absolute_set.add(w)

        #Difference between 1st and 2nd
        second_added_keywords = second_set - first_set
        first_excluded_keywords = first_set - second_set

        if(client != "guangzhou" and client != "beijing-2"):
            second_week_added.append(len(second_added_keywords))
            for w in second_added_keywords:
                second_week_absolute_set.add(w)
            for w in first_excluded_keywords:
                second_week_absolute_set_removed.add(w)

        
        client_differences[client].append([len(second_added_keywords),len(first_excluded_keywords)])

        #Difference between 2nd and 3rd
        third_added_keywords = third_set - second_set
        second_excluded_keywords = second_set - third_set

        third_week_added.append(len(third_added_keywords))
        for w in third_added_keywords:
                third_week_absolute_set.add(w)
        for w in second_excluded_keywords:
                third_week_absolute_set_removed.add(w)

        client_differences[client].append([len(third_added_keywords),len(second_excluded_keywords)])

        #Difference between 3rd and 4th
        fourth_added_keywords = fourth_set - third_set
        third_excluded_keywords = third_set - fourth_set

        fourth_week_added.append(len(fourth_added_keywords))
        for w in fourth_added_keywords:
                fourth_week_absolute_set.add(w)
        for w in third_excluded_keywords:
                fourth_week_absolute_set_removed.add(w)

        client_differences[client].append([len(fourth_added_keywords),len(third_excluded_keywords)])

    #Compute medians
    first_week_added_median = np.median(first_week_added)
    second_week_added_median = np.median(second_week_added) 
    third_week_added_median = np.median(third_week_added) 
    fourth_week_added_median = np.median(fourth_week_added) 

    medians_file = open("longitudinal_medians_%s_%s.txt"%(wordlist, language), 'w')
    """medians_file.write("Distinct keywords added per client, per week:\n")
    medians_file.write(str(sorted(first_week_added)) + "\n")
    medians_file.write(str(sorted(second_week_added)) + "\n")
    medians_file.write(str(sorted(third_week_added)) + "\n")
    medians_file.write(str(sorted(fourth_week_added)) + "\n")
    medians_file.write("\n")

    medians_file.write("Median of keywords added per client, per week:\n")
    medians_file.write(str(first_week_added_median) + "\n")
    medians_file.write(str(second_week_added_median) + "\n")
    medians_file.write(str(third_week_added_median) + "\n")
    medians_file.write(str(fourth_week_added_median) + "\n")
    medians_file.write("\n")"""

    medians_file.write("Total distinct keywords added per week:\n")
    medians_file.write("W1: #:" + str(len(first_week_absolute_set)) + "\n")
    medians_file.write("W2: #:" + str(len(second_week_absolute_set)) + "\n")
    medians_file.write("W3: #:" + str(len(third_week_absolute_set)) +  "\n")
    medians_file.write("W4: #:" + str(len(fourth_week_absolute_set)) +  "\n")
    medians_file.write("Median Added (W2 onward): " + str(np.median([len(second_week_absolute_set),len(third_week_absolute_set),len(fourth_week_absolute_set)])) + "\n")
    medians_file.write("\n")

    medians_file.write("Total distinct keywords removed per week:\n")
    medians_file.write("W1: #:" + str(len(first_week_absolute_set_removed)) + "\n")
    medians_file.write("W2: #:" + str(len(second_week_absolute_set_removed)) + "\n")
    medians_file.write("W3: #:" + str(len(third_week_absolute_set_removed)) +  "\n")
    medians_file.write("W4: #:" + str(len(fourth_week_absolute_set_removed)) +  "\n")
    medians_file.write("Median Removed (W2 onward): " + str(np.median([len(second_week_absolute_set_removed),len(third_week_absolute_set_removed),len(fourth_week_absolute_set_removed)])) + "\n")

    medians_file.close()

    print(client_differences)
    return client_differences



def ComputeTotalDifferencesOverTimeUpdateOnly(language):

    clients = [
        "hk1",
        "hk2",
        "shanghai",
        "beijing-okoze",
        "beijing-2",
        "guangzhou",
        "pittsburgh"
    ]

    client_differences = {}

    first_week_added = []
    second_week_added = []
    third_week_added = []
    fourth_week_added = []

    first_week_absolute_set = set()
    second_week_absolute_set = set()
    third_week_absolute_set = set()
    fourth_week_absolute_set = set()

    first_week_absolute_set_removed = set()
    second_week_absolute_set_removed = set()
    third_week_absolute_set_removed = set()
    fourth_week_absolute_set_removed = set()


    for client in clients:
        client_differences[client] = []

        first_set = set()
        second_set = set()
        third_set = set()
        fourth_set = set()

        first_set_non_minimized = set()
        second_set_non_minimized = set()
        third_set_non_minimized = set()
        fourth_set_non_minimized = set()

        try:
            first = open('../results/http_results/ts-2020-09-11/blocked_keywords/blocked_wikipedia_logs-%s_%s_update_only.txt'%(client,language), 'r')
            first_lines = first.readlines()
            for w in first_lines:
                first_set_non_minimized.add(w)
                word = w.rstrip('\n')
                added_minimization = False
                for minimized_keyword in minimized_wiki_keywords:
                    if(minimized_keyword in word):
                        first_set.add(minimized_keyword)
                        added_minimization = True
                if(not added_minimization):
                    first_set.add(word)
            first.close()

            #print(first_set)

            first = open('../results/http_results/ts-2020-09-11/blocked_keywords/blocked_xia-chu_logs-%s_%s_update_only.txt'%(client,language), 'r')
            first_lines = first.readlines()
            for w in first_lines:
                word = w.rstrip('\n')
                first_set_non_minimized.add(word)
                first_set.add(word)
            first.close()

            #print(first_set)

            first = open('../results/http_results/ts-2020-09-11/blocked_keywords/blocked_chat_logs-%s_%s_update_only.txt'%(client,language), 'r')
            first_lines = first.readlines()

            #Add minimization if exists
            for w in first_lines:
                word = w.rstrip('\n')
                first_set_non_minimized.add(w)
                added_minimization = False
                for minimized_keyword in minimized_chat_keywords:
                    if(minimized_keyword in word):
                        first_set.add(minimized_keyword)
                        print("Found minimzation:")
                        print(minimized_keyword)
                        print(word)
                        added_minimization = True
                if(not added_minimization):
                    first_set.add(word)
            first.close()
            print(first_set)

        except Exception as e:
            print(e)
            pass

        try:
            second = open('../results/http_results/ts-2020-09-18/blocked_keywords/blocked_wikipedia_logs-%s_%s_update_only.txt'%(client,language), 'r')
            second_lines = second.readlines()
            for w in second_lines:
                word = w.rstrip('\n')
                added_minimization = False
                for minimized_keyword in minimized_wiki_keywords:
                    if(minimized_keyword in word):
                        second_set.add(minimized_keyword)
                        added_minimization = True
                if(not added_minimization):
                    second_set.add(word)
            second.close()

            second = open('../results/http_results/ts-2020-09-18/blocked_keywords/blocked_xia-chu_logs-%s_%s_update_only.txt'%(client,language), 'r')
            second_lines = second.readlines()
            for w in second_lines:
                word = w.rstrip('\n')
                second_set.add(word)
            second.close()

            second = open('../results/http_results/ts-2020-09-18/blocked_keywords/blocked_chat_logs-%s_%s_update_only.txt'%(client,language), 'r')
            second_lines = second.readlines()
            for w in second_lines:
                word = w.rstrip('\n')
                added_minimization = False
                for minimized_keyword in minimized_chat_keywords:
                    if(minimized_keyword in word):
                        second_set.add(minimized_keyword)
                        added_minimization = True
                if(not added_minimization):
                    second_set.add(word)
            second.close()
        except:
            pass

        try:
            third = open('../results/http_results/ts-2020-09-28/blocked_keywords/blocked_wikipedia_logs-%s_%s_update_only.txt'%(client,language), 'r')
            third_lines = third.readlines()
            for w in third_lines:
                word = w.rstrip('\n')
                added_minimization = False
                for minimized_keyword in minimized_wiki_keywords:
                    if(minimized_keyword in word):
                        third_set.add(minimized_keyword)
                        added_minimization = True
                if(not added_minimization):
                    third_set.add(word)
            third.close()

            third = open('../results/http_results/ts-2020-09-28/blocked_keywords/blocked_xia-chu_logs-%s_%s_update_only.txt'%(client,language), 'r')
            third_lines = third.readlines()
            for w in third_lines:
                word = w.rstrip('\n')
                third_set.add(word)
            third.close()

            third = open('../results/http_results/ts-2020-09-28/blocked_keywords/blocked_chat_logs-%s_%s_update_only.txt'%(client,language), 'r')
            third_lines = third.readlines()
            for w in third_lines:
                word = w.rstrip('\n')
                added_minimization = False
                for minimized_keyword in minimized_chat_keywords:
                    if(minimized_keyword in word):
                        third_set.add(minimized_keyword)
                        added_minimization = True
                if(not added_minimization):
                    third_set.add(word)
            third.close()
        except:
            pass

        try:
            fourth = open('../results/http_results/ts-2020-10-15/blocked_keywords/blocked_wikipedia_logs-%s_%s_update_only.txt'%(client,language), 'r')
            fourth_lines = fourth.readlines()
            for w in fourth_lines:
                word = w.rstrip('\n')
                added_minimization = False
                for minimized_keyword in minimized_wiki_keywords:
                    if(minimized_keyword in word):
                        fourth_set.add(minimized_keyword)
                        added_minimization = True
                if(not added_minimization):
                    fourth_set.add(word)
            fourth.close()

            fourth = open('../results/http_results/ts-2020-10-15/blocked_keywords/blocked_xia-chu_logs-%s_%s_update_only.txt'%(client,language), 'r')
            fourth_lines = fourth.readlines()
            for w in fourth_lines:
                word = w.rstrip('\n')
                fourth_set.add(word)
            fourth.close()

            fourth = open('../results/http_results/ts-2020-10-15/blocked_keywords/blocked_chat_logs-%s_%s_update_only.txt'%(client,language), 'r')
            fourth_lines = fourth.readlines()
            for w in fourth_lines:
                word = w.rstrip('\n')
                added_minimization = False
                for minimized_keyword in minimized_chat_keywords:
                    if(minimized_keyword in word):
                        fourth_set.add(minimized_keyword)
                        added_minimization = True
                if(not added_minimization):
                    fourth_set.add(word)
            fourth.close()
        except:
            pass
        
        print(client)
        print("1st set (minimized): " + str(len(first_set)) + ", not minimized: " + str(len(first_set_non_minimized)))
        print("2nd set: "+ str(len(second_set)))
        print("3rd set: " + str(len(third_set)))
        print("4th set: " + str(len(fourth_set)))

        #In the first set, we just have the first added keywords
        client_differences[client].append([len(first_set),0])
        
        if(client != "guangzhou" and client != "beijing-2"):
            first_week_added.append(len(first_set))
            for w in first_set:
                first_week_absolute_set.add(w)

        #Difference between 1st and 2nd
        second_added_keywords = second_set - first_set
        first_excluded_keywords = first_set - second_set

        if(client != "guangzhou" and client != "beijing-2"):
            second_week_added.append(len(second_added_keywords))
            for w in second_added_keywords:
                second_week_absolute_set.add(w)
            for w in first_excluded_keywords:
                second_week_absolute_set_removed.add(w)

        
        client_differences[client].append([len(second_added_keywords),len(first_excluded_keywords)])

        #Difference between 2nd and 3rd
        third_added_keywords = third_set - second_set
        second_excluded_keywords = second_set - third_set

        third_week_added.append(len(third_added_keywords))
        for w in third_added_keywords:
                third_week_absolute_set.add(w)
        for w in second_excluded_keywords:
                third_week_absolute_set_removed.add(w)

        client_differences[client].append([len(third_added_keywords),len(second_excluded_keywords)])

        #Difference between 3rd and 4th
        fourth_added_keywords = fourth_set - third_set
        third_excluded_keywords = third_set - fourth_set

        fourth_week_added.append(len(fourth_added_keywords))
        for w in fourth_added_keywords:
                fourth_week_absolute_set.add(w)
        for w in third_excluded_keywords:
                fourth_week_absolute_set_removed.add(w)

        client_differences[client].append([len(fourth_added_keywords),len(third_excluded_keywords)])

    #Compute medians
    first_week_added_median = np.median(first_week_added)
    second_week_added_median = np.median(second_week_added) 
    third_week_added_median = np.median(third_week_added) 
    fourth_week_added_median = np.median(fourth_week_added) 

    medians_file = open("longitudinal_medians_%s_update_only.txt"%(language), 'w')
    medians_file.write("Distinct keywords added per client, per week:\n")
    medians_file.write(str(sorted(first_week_added)) + "\n")
    medians_file.write(str(sorted(second_week_added)) + "\n")
    medians_file.write(str(sorted(third_week_added)) + "\n")
    medians_file.write(str(sorted(fourth_week_added)) + "\n")
    medians_file.write("\n")

    """medians_file.write("Median of keywords added per client, per week:\n")
    medians_file.write(str(first_week_added_median) + "\n")
    medians_file.write(str(second_week_added_median) + "\n")
    medians_file.write(str(third_week_added_median) + "\n")
    medians_file.write(str(fourth_week_added_median) + "\n")
    medians_file.write("\n")"""

    medians_file.write("Total distinct keywords added per week:\n")
    medians_file.write("W1: #:" + str(len(first_week_absolute_set)) + "\n")
    medians_file.write("W2: #:" + str(len(second_week_absolute_set)) + "\n")
    medians_file.write("W3: #:" + str(len(third_week_absolute_set)) +  "\n")
    medians_file.write("W4: #:" + str(len(fourth_week_absolute_set)) +  "\n")
    medians_file.write("Median Added (W2 onward): " + str(np.median([len(second_week_absolute_set),len(third_week_absolute_set),len(fourth_week_absolute_set)])) + "\n")
    medians_file.write("\n")

    medians_file.write("Total distinct keywords removed per week:\n")
    medians_file.write("W1: #:" + str(len(first_week_absolute_set_removed)) + "\n")
    medians_file.write("W2: #:" + str(len(second_week_absolute_set_removed)) + "\n")
    medians_file.write("W3: #:" + str(len(third_week_absolute_set_removed)) +  "\n")
    medians_file.write("W4: #:" + str(len(fourth_week_absolute_set_removed)) +  "\n")
    medians_file.write("Median Removed (W2 onward): " + str(np.median([len(second_week_absolute_set_removed),len(third_week_absolute_set_removed),len(fourth_week_absolute_set_removed)])) + "\n")

    medians_file.close()

    print(client_differences)
    return client_differences


def ComputeTotalDifferencesOverTimeSearchOnly(language):

    clients = [
        "hk1",
        "hk2",
        "shanghai",
        "beijing-okoze",
        "beijing-2",
        "guangzhou",
        "pittsburgh"
    ]

    client_differences = {}

    first_week_added = []
    second_week_added = []
    third_week_added = []
    fourth_week_added = []

    first_week_absolute_set = set()
    second_week_absolute_set = set()
    third_week_absolute_set = set()
    fourth_week_absolute_set = set()

    first_week_absolute_set_removed = set()
    second_week_absolute_set_removed = set()
    third_week_absolute_set_removed = set()
    fourth_week_absolute_set_removed = set()


    for client in clients:
        client_differences[client] = []

        first_set = set()
        second_set = set()
        third_set = set()
        fourth_set = set()

        first_set_non_minimized = set()
        second_set_non_minimized = set()
        third_set_non_minimized = set()
        fourth_set_non_minimized = set()

        try:
            first = open('../results/http_results/ts-2020-09-11/blocked_keywords/blocked_wikipedia_logs-%s_%s_search_only.txt'%(client,language), 'r')
            first_lines = first.readlines()
            for w in first_lines:
                first_set_non_minimized.add(w)
                word = w.rstrip('\n')
                added_minimization = False
                for minimized_keyword in minimized_wiki_keywords:
                    if(minimized_keyword in word):
                        first_set.add(minimized_keyword)
                        added_minimization = True
                if(not added_minimization):
                    first_set.add(word)
            first.close()

            #print(first_set)

            first = open('../results/http_results/ts-2020-09-11/blocked_keywords/blocked_xia-chu_logs-%s_%s_search_only.txt'%(client,language), 'r')
            first_lines = first.readlines()
            for w in first_lines:
                word = w.rstrip('\n')
                first_set_non_minimized.add(word)
                first_set.add(word)
            first.close()

            #print(first_set)

            first = open('../results/http_results/ts-2020-09-11/blocked_keywords/blocked_chat_logs-%s_%s_search_only.txt'%(client,language), 'r')
            first_lines = first.readlines()

            #Add minimization if exists
            for w in first_lines:
                word = w.rstrip('\n')
                first_set_non_minimized.add(w)
                added_minimization = False
                for minimized_keyword in minimized_chat_keywords:
                    if(minimized_keyword in word):
                        first_set.add(minimized_keyword)
                        print("Found minimzation:")
                        print(minimized_keyword)
                        print(word)
                        added_minimization = True
                if(not added_minimization):
                    first_set.add(word)
            first.close()
            print(first_set)

        except Exception as e:
            print(e)
            pass

        try:
            second = open('../results/http_results/ts-2020-09-18/blocked_keywords/blocked_wikipedia_logs-%s_%s_search_only.txt'%(client,language), 'r')
            second_lines = second.readlines()
            for w in second_lines:
                word = w.rstrip('\n')
                added_minimization = False
                for minimized_keyword in minimized_wiki_keywords:
                    if(minimized_keyword in word):
                        second_set.add(minimized_keyword)
                        added_minimization = True
                if(not added_minimization):
                    second_set.add(word)
            second.close()

            second = open('../results/http_results/ts-2020-09-18/blocked_keywords/blocked_xia-chu_logs-%s_%s_search_only.txt'%(client,language), 'r')
            second_lines = second.readlines()
            for w in second_lines:
                word = w.rstrip('\n')
                second_set.add(word)
            second.close()

            second = open('../results/http_results/ts-2020-09-18/blocked_keywords/blocked_chat_logs-%s_%s_search_only.txt'%(client,language), 'r')
            second_lines = second.readlines()
            for w in second_lines:
                word = w.rstrip('\n')
                added_minimization = False
                for minimized_keyword in minimized_chat_keywords:
                    if(minimized_keyword in word):
                        second_set.add(minimized_keyword)
                        added_minimization = True
                if(not added_minimization):
                    second_set.add(word)
            second.close()
        except:
            pass

        try:
            third = open('../results/http_results/ts-2020-09-28/blocked_keywords/blocked_wikipedia_logs-%s_%s_search_only.txt'%(client,language), 'r')
            third_lines = third.readlines()
            for w in third_lines:
                word = w.rstrip('\n')
                added_minimization = False
                for minimized_keyword in minimized_wiki_keywords:
                    if(minimized_keyword in word):
                        third_set.add(minimized_keyword)
                        added_minimization = True
                if(not added_minimization):
                    third_set.add(word)
            third.close()

            third = open('../results/http_results/ts-2020-09-28/blocked_keywords/blocked_xia-chu_logs-%s_%s_search_only.txt'%(client,language), 'r')
            third_lines = third.readlines()
            for w in third_lines:
                word = w.rstrip('\n')
                third_set.add(word)
            third.close()

            third = open('../results/http_results/ts-2020-09-28/blocked_keywords/blocked_chat_logs-%s_%s_search_only.txt'%(client,language), 'r')
            third_lines = third.readlines()
            for w in third_lines:
                word = w.rstrip('\n')
                added_minimization = False
                for minimized_keyword in minimized_chat_keywords:
                    if(minimized_keyword in word):
                        third_set.add(minimized_keyword)
                        added_minimization = True
                if(not added_minimization):
                    third_set.add(word)
            third.close()
        except:
            pass

        try:
            fourth = open('../results/http_results/ts-2020-10-15/blocked_keywords/blocked_wikipedia_logs-%s_%s_search_only.txt'%(client,language), 'r')
            fourth_lines = fourth.readlines()
            for w in fourth_lines:
                word = w.rstrip('\n')
                added_minimization = False
                for minimized_keyword in minimized_wiki_keywords:
                    if(minimized_keyword in word):
                        fourth_set.add(minimized_keyword)
                        added_minimization = True
                if(not added_minimization):
                    fourth_set.add(word)
            fourth.close()

            fourth = open('../results/http_results/ts-2020-10-15/blocked_keywords/blocked_xia-chu_logs-%s_%s_search_only.txt'%(client,language), 'r')
            fourth_lines = fourth.readlines()
            for w in fourth_lines:
                word = w.rstrip('\n')
                fourth_set.add(word)
            fourth.close()

            fourth = open('../results/http_results/ts-2020-10-15/blocked_keywords/blocked_chat_logs-%s_%s_search_only.txt'%(client,language), 'r')
            fourth_lines = fourth.readlines()
            for w in fourth_lines:
                word = w.rstrip('\n')
                added_minimization = False
                for minimized_keyword in minimized_chat_keywords:
                    if(minimized_keyword in word):
                        fourth_set.add(minimized_keyword)
                        added_minimization = True
                if(not added_minimization):
                    fourth_set.add(word)
            fourth.close()
        except:
            pass
        
        print(client)
        print("1st set (minimized): " + str(len(first_set)) + ", not minimized: " + str(len(first_set_non_minimized)))
        print("2nd set: "+ str(len(second_set)))
        print("3rd set: " + str(len(third_set)))
        print("4th set: " + str(len(fourth_set)))

        #In the first set, we just have the first added keywords
        client_differences[client].append([len(first_set),0])
        
        if(client != "guangzhou" and client != "beijing-2"):
            first_week_added.append(len(first_set))
            for w in first_set:
                first_week_absolute_set.add(w)

        #Difference between 1st and 2nd
        second_added_keywords = second_set - first_set
        first_excluded_keywords = first_set - second_set

        if(client != "guangzhou" and client != "beijing-2"):
            second_week_added.append(len(second_added_keywords))
            for w in second_added_keywords:
                second_week_absolute_set.add(w)
            for w in first_excluded_keywords:
                second_week_absolute_set_removed.add(w)

        
        client_differences[client].append([len(second_added_keywords),len(first_excluded_keywords)])

        #Difference between 2nd and 3rd
        third_added_keywords = third_set - second_set
        second_excluded_keywords = second_set - third_set

        third_week_added.append(len(third_added_keywords))
        for w in third_added_keywords:
                third_week_absolute_set.add(w)
        for w in second_excluded_keywords:
                third_week_absolute_set_removed.add(w)

        client_differences[client].append([len(third_added_keywords),len(second_excluded_keywords)])

        #Difference between 3rd and 4th
        fourth_added_keywords = fourth_set - third_set
        third_excluded_keywords = third_set - fourth_set

        fourth_week_added.append(len(fourth_added_keywords))
        for w in fourth_added_keywords:
                fourth_week_absolute_set.add(w)
        for w in third_excluded_keywords:
                fourth_week_absolute_set_removed.add(w)

        client_differences[client].append([len(fourth_added_keywords),len(third_excluded_keywords)])

    #Compute medians
    first_week_added_median = np.median(first_week_added)
    second_week_added_median = np.median(second_week_added) 
    third_week_added_median = np.median(third_week_added) 
    fourth_week_added_median = np.median(fourth_week_added) 

    medians_file = open("longitudinal_medians_%s_search_only.txt"%(language), 'w')
    medians_file.write("Distinct keywords added per client, per week:\n")
    medians_file.write(str(sorted(first_week_added)) + "\n")
    medians_file.write(str(sorted(second_week_added)) + "\n")
    medians_file.write(str(sorted(third_week_added)) + "\n")
    medians_file.write(str(sorted(fourth_week_added)) + "\n")
    medians_file.write("\n")

    """medians_file.write("Median of keywords added per client, per week:\n")
    medians_file.write(str(first_week_added_median) + "\n")
    medians_file.write(str(second_week_added_median) + "\n")
    medians_file.write(str(third_week_added_median) + "\n")
    medians_file.write(str(fourth_week_added_median) + "\n")
    medians_file.write("\n")"""

    medians_file.write("Total distinct keywords added per week:\n")
    medians_file.write("W1: #:" + str(len(first_week_absolute_set)) + "\n")
    medians_file.write("W2: #:" + str(len(second_week_absolute_set)) + "\n")
    medians_file.write("W3: #:" + str(len(third_week_absolute_set)) +  "\n")
    medians_file.write("W4: #:" + str(len(fourth_week_absolute_set)) +  "\n")
    medians_file.write("Median Added (W2 onward): " + str(np.median([len(second_week_absolute_set),len(third_week_absolute_set),len(fourth_week_absolute_set)])) + "\n")
    medians_file.write("\n")

    medians_file.write("Total distinct keywords removed per week:\n")
    medians_file.write("W1: #:" + str(len(first_week_absolute_set_removed)) + "\n")
    medians_file.write("W2: #:" + str(len(second_week_absolute_set_removed)) + "\n")
    medians_file.write("W3: #:" + str(len(third_week_absolute_set_removed)) +  "\n")
    medians_file.write("W4: #:" + str(len(fourth_week_absolute_set_removed)) +  "\n")
    medians_file.write("Median Removed (W2 onward): " + str(np.median([len(second_week_absolute_set_removed),len(third_week_absolute_set_removed),len(fourth_week_absolute_set_removed)])) + "\n")

    medians_file.close()

    print(client_differences)
    return client_differences


def ComputeTotalDifferencesOverTime(language):

    clients = [
        "hk1",
        "hk2",
        "shanghai",
        "beijing-okoze",
        "beijing-2",
        "guangzhou",
        "pittsburgh"
    ]

    client_differences = {}

    first_week_added = []
    second_week_added = []
    third_week_added = []
    fourth_week_added = []

    first_week_absolute_set = set()
    second_week_absolute_set = set()
    third_week_absolute_set = set()
    fourth_week_absolute_set = set()

    first_week_absolute_set_removed = set()
    second_week_absolute_set_removed = set()
    third_week_absolute_set_removed = set()
    fourth_week_absolute_set_removed = set()


    for client in clients:
        client_differences[client] = []

        first_set = set()
        second_set = set()
        third_set = set()
        fourth_set = set()

        first_set_non_minimized = set()
        second_set_non_minimized = set()
        third_set_non_minimized = set()
        fourth_set_non_minimized = set()

        try:
            first = open('../results/http_results/ts-2020-09-11/blocked_keywords/blocked_wikipedia_logs-%s_%s.txt'%(client,language), 'r')
            first_lines = first.readlines()
            for w in first_lines:
                first_set_non_minimized.add(w)
                word = w.rstrip('\n')
                added_minimization = False
                for minimized_keyword in minimized_wiki_keywords:
                    if(minimized_keyword in word):
                        first_set.add(minimized_keyword)
                        added_minimization = True
                if(not added_minimization):
                    first_set.add(word)
            first.close()

            #print(first_set)

            first = open('../results/http_results/ts-2020-09-11/blocked_keywords/blocked_xia-chu_logs-%s_%s.txt'%(client,language), 'r')
            first_lines = first.readlines()
            for w in first_lines:
                word = w.rstrip('\n')
                first_set_non_minimized.add(word)
                first_set.add(word)
            first.close()

            #print(first_set)

            first = open('../results/http_results/ts-2020-09-11/blocked_keywords/blocked_chat_logs-%s_%s.txt'%(client,language), 'r')
            first_lines = first.readlines()

            #Add minimization if exists
            for w in first_lines:
                word = w.rstrip('\n')
                first_set_non_minimized.add(w)
                added_minimization = False
                for minimized_keyword in minimized_chat_keywords:
                    if(minimized_keyword in word):
                        first_set.add(minimized_keyword)
                        print("Found minimzation:")
                        print(minimized_keyword)
                        print(word)
                        added_minimization = True
                if(not added_minimization):
                    first_set.add(word)
            first.close()
            print(first_set)

        except Exception as e:
            print(e)
            pass

        try:
            second = open('../results/http_results/ts-2020-09-18/blocked_keywords/blocked_wikipedia_logs-%s_%s.txt'%(client,language), 'r')
            second_lines = second.readlines()
            for w in second_lines:
                word = w.rstrip('\n')
                added_minimization = False
                for minimized_keyword in minimized_wiki_keywords:
                    if(minimized_keyword in word):
                        second_set.add(minimized_keyword)
                        added_minimization = True
                if(not added_minimization):
                    second_set.add(word)
            second.close()

            second = open('../results/http_results/ts-2020-09-18/blocked_keywords/blocked_xia-chu_logs-%s_%s.txt'%(client,language), 'r')
            second_lines = second.readlines()
            for w in second_lines:
                word = w.rstrip('\n')
                second_set.add(word)
            second.close()

            second = open('../results/http_results/ts-2020-09-18/blocked_keywords/blocked_chat_logs-%s_%s.txt'%(client,language), 'r')
            second_lines = second.readlines()
            for w in second_lines:
                word = w.rstrip('\n')
                added_minimization = False
                for minimized_keyword in minimized_chat_keywords:
                    if(minimized_keyword in word):
                        second_set.add(minimized_keyword)
                        added_minimization = True
                if(not added_minimization):
                    second_set.add(word)
            second.close()
        except:
            pass

        try:
            third = open('../results/http_results/ts-2020-09-28/blocked_keywords/blocked_wikipedia_logs-%s_%s.txt'%(client,language), 'r')
            third_lines = third.readlines()
            for w in third_lines:
                word = w.rstrip('\n')
                added_minimization = False
                for minimized_keyword in minimized_wiki_keywords:
                    if(minimized_keyword in word):
                        third_set.add(minimized_keyword)
                        added_minimization = True
                if(not added_minimization):
                    third_set.add(word)
            third.close()

            third = open('../results/http_results/ts-2020-09-28/blocked_keywords/blocked_xia-chu_logs-%s_%s.txt'%(client,language), 'r')
            third_lines = third.readlines()
            for w in third_lines:
                word = w.rstrip('\n')
                third_set.add(word)
            third.close()

            third = open('../results/http_results/ts-2020-09-28/blocked_keywords/blocked_chat_logs-%s_%s.txt'%(client,language), 'r')
            third_lines = third.readlines()
            for w in third_lines:
                word = w.rstrip('\n')
                added_minimization = False
                for minimized_keyword in minimized_chat_keywords:
                    if(minimized_keyword in word):
                        third_set.add(minimized_keyword)
                        added_minimization = True
                if(not added_minimization):
                    third_set.add(word)
            third.close()
        except:
            pass

        try:
            fourth = open('../results/http_results/ts-2020-10-15/blocked_keywords/blocked_wikipedia_logs-%s_%s.txt'%(client,language), 'r')
            fourth_lines = fourth.readlines()
            for w in fourth_lines:
                word = w.rstrip('\n')
                added_minimization = False
                for minimized_keyword in minimized_wiki_keywords:
                    if(minimized_keyword in word):
                        fourth_set.add(minimized_keyword)
                        added_minimization = True
                if(not added_minimization):
                    fourth_set.add(word)
            fourth.close()

            fourth = open('../results/http_results/ts-2020-10-15/blocked_keywords/blocked_xia-chu_logs-%s_%s.txt'%(client,language), 'r')
            fourth_lines = fourth.readlines()
            for w in fourth_lines:
                word = w.rstrip('\n')
                fourth_set.add(word)
            fourth.close()

            fourth = open('../results/http_results/ts-2020-10-15/blocked_keywords/blocked_chat_logs-%s_%s.txt'%(client,language), 'r')
            fourth_lines = fourth.readlines()
            for w in fourth_lines:
                word = w.rstrip('\n')
                added_minimization = False
                for minimized_keyword in minimized_chat_keywords:
                    if(minimized_keyword in word):
                        fourth_set.add(minimized_keyword)
                        added_minimization = True
                if(not added_minimization):
                    fourth_set.add(word)
            fourth.close()
        except:
            pass
        
        print(client)
        print("1st set (minimized): " + str(len(first_set)) + ", not minimized: " + str(len(first_set_non_minimized)))
        print("2nd set: "+ str(len(second_set)))
        print("3rd set: " + str(len(third_set)))
        print("4th set: " + str(len(fourth_set)))

        #In the first set, we just have the first added keywords
        client_differences[client].append([len(first_set),0])
        
        if(client != "guangzhou" and client != "beijing-2"):
            first_week_added.append(len(first_set))
            for w in first_set:
                first_week_absolute_set.add(w)

        #Difference between 1st and 2nd
        second_added_keywords = second_set - first_set
        first_excluded_keywords = first_set - second_set

        if(client != "guangzhou" and client != "beijing-2"):
            second_week_added.append(len(second_added_keywords))
            for w in second_added_keywords:
                second_week_absolute_set.add(w)
            for w in first_excluded_keywords:
                second_week_absolute_set_removed.add(w)

        
        client_differences[client].append([len(second_added_keywords),len(first_excluded_keywords)])

        #Difference between 2nd and 3rd
        third_added_keywords = third_set - second_set
        second_excluded_keywords = second_set - third_set

        third_week_added.append(len(third_added_keywords))
        for w in third_added_keywords:
                third_week_absolute_set.add(w)
        for w in second_excluded_keywords:
                third_week_absolute_set_removed.add(w)

        client_differences[client].append([len(third_added_keywords),len(second_excluded_keywords)])

        #Difference between 3rd and 4th
        fourth_added_keywords = fourth_set - third_set
        third_excluded_keywords = third_set - fourth_set

        fourth_week_added.append(len(fourth_added_keywords))
        for w in fourth_added_keywords:
                fourth_week_absolute_set.add(w)
        for w in third_excluded_keywords:
                fourth_week_absolute_set_removed.add(w)

        client_differences[client].append([len(fourth_added_keywords),len(third_excluded_keywords)])

    #Compute medians
    first_week_added_median = np.median(first_week_added)
    second_week_added_median = np.median(second_week_added) 
    third_week_added_median = np.median(third_week_added) 
    fourth_week_added_median = np.median(fourth_week_added) 

    medians_file = open("longitudinal_medians_%s.txt"%(language), 'w')
    medians_file.write("Distinct keywords added per client, per week:\n")
    medians_file.write(str(sorted(first_week_added)) + "\n")
    medians_file.write(str(sorted(second_week_added)) + "\n")
    medians_file.write(str(sorted(third_week_added)) + "\n")
    medians_file.write(str(sorted(fourth_week_added)) + "\n")
    medians_file.write("\n")

    """medians_file.write("Median of keywords added per client, per week:\n")
    medians_file.write(str(first_week_added_median) + "\n")
    medians_file.write(str(second_week_added_median) + "\n")
    medians_file.write(str(third_week_added_median) + "\n")
    medians_file.write(str(fourth_week_added_median) + "\n")
    medians_file.write("\n")"""

    medians_file.write("Total distinct keywords added per week:\n")
    medians_file.write("W1: #:" + str(len(first_week_absolute_set)) + "\n")
    medians_file.write("W2: #:" + str(len(second_week_absolute_set)) + "\n")
    medians_file.write("W3: #:" + str(len(third_week_absolute_set)) +  "\n")
    medians_file.write("W4: #:" + str(len(fourth_week_absolute_set)) +  "\n")
    medians_file.write("Median Added (W2 onward): " + str(np.median([len(second_week_absolute_set),len(third_week_absolute_set),len(fourth_week_absolute_set)])) + "\n")
    medians_file.write("\n")

    medians_file.write("Total distinct keywords removed per week:\n")
    medians_file.write("W1: #:" + str(len(first_week_absolute_set_removed)) + "\n")
    medians_file.write("W2: #:" + str(len(second_week_absolute_set_removed)) + "\n")
    medians_file.write("W3: #:" + str(len(third_week_absolute_set_removed)) +  "\n")
    medians_file.write("W4: #:" + str(len(fourth_week_absolute_set_removed)) +  "\n")
    medians_file.write("Median Removed (W2 onward): " + str(np.median([len(second_week_absolute_set_removed),len(third_week_absolute_set_removed),len(fourth_week_absolute_set_removed)])) + "\n")

    medians_file.close()

    print(client_differences)
    return client_differences


def PlotLongitudinal(client_evolution_over_time, wordlist):

    fig, ax = plt.subplots(7, 1, sharex=True, figsize=[6, 8])

    labels = [" ", " ", " "]
    x = np.arange(len(labels))
    width = 0.35

    #HK1
    additions = []
    additions.append(client_evolution_over_time["hk1"][1][0])
    additions.append(client_evolution_over_time["hk1"][2][0])
    additions.append(client_evolution_over_time["hk1"][3][0])

    removed = []
    removed.append(-client_evolution_over_time["hk1"][1][1])
    removed.append(-client_evolution_over_time["hk1"][2][1])
    removed.append(-client_evolution_over_time["hk1"][3][1])

    ax[0].bar(x - width/2, additions, width, label='Added')
    ax[0].bar(x + width/2, removed, width, label='Removed')

    plt.setp(ax[0], ylabel='HK (1)')
    #ax[0].set_title('Hong Kong (1)')

    #HK2
    additions = []
    #additions.append(client_evolution_over_time["hk2"][0][0])
    additions.append(client_evolution_over_time["hk2"][1][0])
    additions.append(client_evolution_over_time["hk2"][2][0])
    additions.append(client_evolution_over_time["hk2"][3][0])

    removed = []
    #removed.append(client_evolution_over_time["hk2"][0][1])
    removed.append(-client_evolution_over_time["hk2"][1][1])
    removed.append(-client_evolution_over_time["hk2"][2][1])
    removed.append(-client_evolution_over_time["hk2"][3][1])

    ax[1].bar(x - width/2, additions, width, label='Added')
    ax[1].bar(x + width/2, removed, width, label='Removed')

    plt.setp(ax[1], ylabel='HK (2)')
    #ax[1].set_title('Hong Kong (2)') 

    #Shanghai
    additions = []
    #additions.append(client_evolution_over_time["shanghai"][0][0])
    additions.append(client_evolution_over_time["shanghai"][1][0])
    additions.append(client_evolution_over_time["shanghai"][2][0])
    additions.append(client_evolution_over_time["shanghai"][3][0])

    removed = []
    #removed.append(client_evolution_over_time["shanghai"][0][1])
    removed.append(-client_evolution_over_time["shanghai"][1][1])
    removed.append(-client_evolution_over_time["shanghai"][2][1])
    removed.append(-client_evolution_over_time["shanghai"][3][1])

    ax[2].bar(x - width/2, additions, width, label='Added')
    ax[2].bar(x + width/2, removed, width, label='Removed')

    plt.setp(ax[2], ylabel='Shanghai')
    #ax[2].set_title('Shanghai')

    #Beijing (1)
    additions = []
    #additions.append(client_evolution_over_time["beijing-okoze"][0][0])
    additions.append(client_evolution_over_time["beijing-okoze"][1][0])
    additions.append(client_evolution_over_time["beijing-okoze"][2][0])
    additions.append(client_evolution_over_time["beijing-okoze"][3][0])

    removed = []
    #removed.append(client_evolution_over_time["beijing-okoze"][0][1])
    removed.append(-client_evolution_over_time["beijing-okoze"][1][1])
    removed.append(-client_evolution_over_time["beijing-okoze"][2][1])
    removed.append(-client_evolution_over_time["beijing-okoze"][3][1])

    ax[3].bar(x - width/2, additions, width, label='Added')
    ax[3].bar(x + width/2, removed, width, label='Removed')

    plt.setp(ax[3], ylabel='Beijing (1)')
    #ax[3].set_title('Beijing(1)')

    #Beijing (2)
    additions = []
    #additions.append(client_evolution_over_time["beijing-2"][0][0])
    additions.append(0)
    additions.append(0)
    additions.append(client_evolution_over_time["beijing-2"][3][0])

    removed = []
    #removed.append(client_evolution_over_time["beijing-2"][0][1])
    removed.append(0)
    removed.append(0)
    removed.append(client_evolution_over_time["beijing-2"][3][1])

    ax[4].bar(x - width/2, additions, width, label='Added')
    ax[4].bar(x + width/2, removed, width, label='Removed')

    plt.setp(ax[4], ylabel='Beijing (2)')
    #ax[4].set_title('Beijing(2)')

    #Guangzhou
    additions = []
    #additions.append(client_evolution_over_time["guangzhou"][0][0])
    additions.append(0)
    additions.append(0)
    additions.append(client_evolution_over_time["guangzhou"][3][0])

    removed = []
    #removed.append(client_evolution_over_time["guangzhou"][0][1])
    removed.append(0)
    removed.append(0)
    removed.append(client_evolution_over_time["guangzhou"][3][1])

    ax[5].bar(x - width/2, additions, width, label='Added')
    ax[5].bar(x + width/2, removed, width, label='Removed')

    plt.setp(ax[5], ylabel='Guangzhou')
    #ax[5].set_title('Guangzhou')

    #Pittsburgh
    additions = []
    #additions.append(client_evolution_over_time["pittsburgh"][0][0])
    additions.append(client_evolution_over_time["pittsburgh"][1][0])
    additions.append(client_evolution_over_time["pittsburgh"][2][0])
    additions.append(client_evolution_over_time["pittsburgh"][3][0])

    removed = []
    #removed.append(client_evolution_over_time["pittsburgh"][0][1])
    removed.append(-client_evolution_over_time["pittsburgh"][1][1])
    removed.append(-client_evolution_over_time["pittsburgh"][2][1])
    removed.append(-client_evolution_over_time["pittsburgh"][3][1])

    ax[6].bar(x - width/2, additions, width, label='Added')
    ax[6].bar(x + width/2, removed, width, label='Removed')
    #ax[6].set_title('[redacted], PA')
    plt.setp(ax[6], ylabel='PA')

    ax[0].spines['right'].set_visible(False)
    ax[0].spines['top'].set_visible(False)
    ax[0].spines['left'].set_visible(False)
    ax[0].spines['bottom'].set_visible(False)
    ax[1].spines['right'].set_visible(False)
    ax[1].spines['top'].set_visible(False)
    ax[1].spines['left'].set_visible(False)
    ax[1].spines['bottom'].set_visible(False)
    ax[2].spines['right'].set_visible(False)
    ax[2].spines['top'].set_visible(False)
    ax[2].spines['left'].set_visible(False)
    ax[2].spines['bottom'].set_visible(False)
    ax[3].spines['right'].set_visible(False)
    ax[3].spines['top'].set_visible(False)
    ax[3].spines['left'].set_visible(False)
    ax[3].spines['bottom'].set_visible(False)
    ax[4].spines['right'].set_visible(False)
    ax[4].spines['top'].set_visible(False)
    ax[4].spines['left'].set_visible(False)
    ax[4].spines['bottom'].set_visible(False)
    ax[5].spines['right'].set_visible(False)
    ax[5].spines['top'].set_visible(False)
    ax[5].spines['left'].set_visible(False)
    ax[5].spines['bottom'].set_visible(False)
    ax[6].spines['right'].set_visible(False)
    ax[6].spines['top'].set_visible(False)
    ax[6].spines['left'].set_visible(False)
    ax[6].spines['bottom'].set_visible(False)

    ax[0].tick_params(bottom=False)
    ticks =  ax[0].get_yticks()
    ax[0].set_yticklabels([int(abs(tick)) for tick in ticks])
    ax[0].axhline(y=0, xmin=0, xmax=3, color="black", lw=0.75)

    ax[1].tick_params(bottom=False)
    ticks =  ax[1].get_yticks()
    ax[1].set_yticklabels([int(abs(tick)) for tick in ticks])
    ax[1].axhline(y=0, xmin=0, xmax=3, color="black", lw=0.75)

    ax[2].tick_params(bottom=False)
    ticks =  ax[2].get_yticks()
    ax[2].set_yticklabels([int(abs(tick)) for tick in ticks])
    ax[2].axhline(y=0, xmin=0, xmax=3, color="black", lw=0.75)

    ax[3].tick_params(bottom=False)
    ticks =  ax[3].get_yticks()
    ax[3].set_yticklabels([int(abs(tick)) for tick in ticks])
    ax[3].axhline(y=0, xmin=0, xmax=3, color="black", lw=0.75)

    ax[4].tick_params(bottom=False)
    ticks =  ax[4].get_yticks()
    ax[4].set_yticklabels([int(abs(tick)) for tick in ticks])
    ax[4].axhline(y=0, xmin=0, xmax=3, color="black", lw=0.75)

    ax[5].tick_params(bottom=False)
    ticks =  ax[5].get_yticks()
    ax[5].set_yticklabels([int(abs(tick)) for tick in ticks])
    ax[5].axhline(y=0, xmin=0, xmax=3, color="black", lw=0.75)

    #ax[6].tick_params(bottom=False)
    ticks =  ax[6].get_yticks()
    ax[6].set_yticklabels([int(abs(tick)) for tick in ticks])
    ax[6].axhline(y=0, xmin=0, xmax=3, color="black", lw=0.75)


    plt.subplots_adjust(wspace=0.4, hspace=0.6)
    # Put a legend on top of current axis
    ax[6].legend(loc='upper center', bbox_to_anchor=(0.45, -.45),
          fancybox=True, shadow=False, ncol=3, fontsize=12)

    #plt.setp(ax1.get_xticklabels(), fontsize=20)
    #plt.setp(ax1.get_yticklabels(), fontsize=20)
    plt.xticks(np.arange(4),labels, fontsize=8)
    ax[6].set(xlim=(-0.5, 2.5))

    """if(wordlist == "wikipedia"):
        ax[0].set(ylim=(0, 15))
        ax[1].set(ylim=(0, 15))
        ax[2].set(ylim=(0, 15))
        ax[3].set(ylim=(0, 15))
        ax[4].set(ylim=(0, 15))
        ax[5].set(ylim=(0, 15))
        ax[6].set(ylim=(0, 15))"""

    #plt.tight_layout()
           
    fig.savefig("longitudinal_%s_%s.pdf"%(wordlist, language))   # save the figure to file
    plt.close(fig)


def PlotTotalLongitudinal(client_evolution_over_time,language):

    fig, ax = plt.subplots(7, 1, sharex=True, figsize=[6, 8])

    labels = [" ", " ", " "]
    x = np.arange(len(labels))
    width = 0.35

    #HK1
    additions = []
    additions.append(client_evolution_over_time["hk1"][1][0])
    additions.append(client_evolution_over_time["hk1"][2][0])
    additions.append(client_evolution_over_time["hk1"][3][0])

    removed = []
    removed.append(-client_evolution_over_time["hk1"][1][1])
    removed.append(-client_evolution_over_time["hk1"][2][1])
    removed.append(-client_evolution_over_time["hk1"][3][1])

    ax[0].bar(x - width/2, additions, width, label='Added', color=colors[0])
    ax[0].bar(x + width/2, removed, width, label='Removed', color=colors[1])

    plt.setp(ax[0], ylabel='HK (1)')
    #ax[0].set_title('Hong Kong (1)')

    #HK2
    additions = []
    #additions.append(client_evolution_over_time["hk2"][0][0])
    additions.append(client_evolution_over_time["hk2"][1][0])
    additions.append(client_evolution_over_time["hk2"][2][0])
    additions.append(client_evolution_over_time["hk2"][3][0])

    removed = []
    #removed.append(client_evolution_over_time["hk2"][0][1])
    removed.append(-client_evolution_over_time["hk2"][1][1])
    removed.append(-client_evolution_over_time["hk2"][2][1])
    removed.append(-client_evolution_over_time["hk2"][3][1])

    ax[1].bar(x - width/2, additions, width, label='Added', color=colors[0])
    ax[1].bar(x + width/2, removed, width, label='Removed', color=colors[1])

    plt.setp(ax[1], ylabel='HK (2)')
    #ax[1].set_title('Hong Kong (2)') 

    #Shanghai
    additions = []
    #additions.append(client_evolution_over_time["shanghai"][0][0])
    additions.append(client_evolution_over_time["shanghai"][1][0])
    additions.append(client_evolution_over_time["shanghai"][2][0])
    additions.append(client_evolution_over_time["shanghai"][3][0])

    removed = []
    #removed.append(client_evolution_over_time["shanghai"][0][1])
    removed.append(-client_evolution_over_time["shanghai"][1][1])
    removed.append(-client_evolution_over_time["shanghai"][2][1])
    removed.append(-client_evolution_over_time["shanghai"][3][1])

    ax[2].bar(x - width/2, additions, width, label='Added', color=colors[0])
    ax[2].bar(x + width/2, removed, width, label='Removed', color=colors[1])

    plt.setp(ax[2], ylabel='Shanghai')
    #ax[2].set_title('Shanghai')

    #Beijing (1)
    additions = []
    #additions.append(client_evolution_over_time["beijing-okoze"][0][0])
    additions.append(client_evolution_over_time["beijing-okoze"][1][0])
    additions.append(client_evolution_over_time["beijing-okoze"][2][0])
    additions.append(client_evolution_over_time["beijing-okoze"][3][0])

    removed = []
    #removed.append(client_evolution_over_time["beijing-okoze"][0][1])
    removed.append(-client_evolution_over_time["beijing-okoze"][1][1])
    removed.append(-client_evolution_over_time["beijing-okoze"][2][1])
    removed.append(-client_evolution_over_time["beijing-okoze"][3][1])

    ax[3].bar(x - width/2, additions, width, label='Added', color=colors[0])
    ax[3].bar(x + width/2, removed, width, label='Removed', color=colors[1])

    plt.setp(ax[3], ylabel='Beijing (1)')
    #ax[3].set_title('Beijing(1)')

    #Beijing (2)
    additions = []
    #additions.append(client_evolution_over_time["beijing-2"][0][0])
    additions.append(0)
    additions.append(0)
    additions.append(client_evolution_over_time["beijing-2"][3][0])

    removed = []
    #removed.append(client_evolution_over_time["beijing-2"][0][1])
    removed.append(0)
    removed.append(0)
    removed.append(-client_evolution_over_time["beijing-2"][3][1])

    ax[4].bar(x - width/2, additions, width, label='Added', color=colors[0])
    ax[4].bar(x + width/2, removed, width, label='Removed', color=colors[1])

    plt.setp(ax[4], ylabel='Beijing (2)')
    #ax[4].set_title('Beijing(2)')

    #Guangzhou
    additions = []
    #additions.append(client_evolution_over_time["guangzhou"][0][0])
    additions.append(0)
    additions.append(0)
    additions.append(client_evolution_over_time["guangzhou"][3][0])

    removed = []
    #removed.append(client_evolution_over_time["guangzhou"][0][1])
    removed.append(0)
    removed.append(0)
    removed.append(-client_evolution_over_time["guangzhou"][3][1])

    ax[5].bar(x - width/2, additions, width, label='Added', color=colors[0])
    ax[5].bar(x + width/2, removed, width, label='Removed', color=colors[1])

    plt.setp(ax[5], ylabel='Guangzhou')
    #ax[5].set_title('Guangzhou')

    #Pittsburgh
    additions = []
    #additions.append(client_evolution_over_time["pittsburgh"][0][0])
    additions.append(client_evolution_over_time["pittsburgh"][1][0])
    additions.append(client_evolution_over_time["pittsburgh"][2][0])
    additions.append(client_evolution_over_time["pittsburgh"][3][0])

    removed = []
    #removed.append(client_evolution_over_time["pittsburgh"][0][1])
    removed.append(-client_evolution_over_time["pittsburgh"][1][1])
    removed.append(-client_evolution_over_time["pittsburgh"][2][1])
    removed.append(-client_evolution_over_time["pittsburgh"][3][1])

    ax[6].bar(x - width/2, additions, width, label='Added', color=colors[0])
    ax[6].bar(x + width/2, removed, width, label='Removed', color=colors[1])
    #ax[6].set_title('[redacted], PA')
    plt.setp(ax[6], ylabel='PA')

    ax[0].spines['right'].set_visible(False)
    ax[0].spines['top'].set_visible(False)
    ax[0].spines['left'].set_visible(False)
    ax[0].spines['bottom'].set_visible(False)
    ax[1].spines['right'].set_visible(False)
    ax[1].spines['top'].set_visible(False)
    ax[1].spines['left'].set_visible(False)
    ax[1].spines['bottom'].set_visible(False)
    ax[2].spines['right'].set_visible(False)
    ax[2].spines['top'].set_visible(False)
    ax[2].spines['left'].set_visible(False)
    ax[2].spines['bottom'].set_visible(False)
    ax[3].spines['right'].set_visible(False)
    ax[3].spines['top'].set_visible(False)
    ax[3].spines['left'].set_visible(False)
    ax[3].spines['bottom'].set_visible(False)
    ax[4].spines['right'].set_visible(False)
    ax[4].spines['top'].set_visible(False)
    ax[4].spines['left'].set_visible(False)
    ax[4].spines['bottom'].set_visible(False)
    ax[5].spines['right'].set_visible(False)
    ax[5].spines['top'].set_visible(False)
    ax[5].spines['left'].set_visible(False)
    ax[5].spines['bottom'].set_visible(False)
    ax[6].spines['right'].set_visible(False)
    ax[6].spines['top'].set_visible(False)
    ax[6].spines['left'].set_visible(False)
    ax[6].spines['bottom'].set_visible(False)

    ax[0].tick_params(bottom=False)
    ticks =  ax[0].get_yticks()
    ax[0].set_yticklabels([int(abs(tick)) for tick in ticks])
    ax[0].axhline(y=0, xmin=0, xmax=3, color="black", lw=0.75)

    ax[1].tick_params(bottom=False)
    ticks =  ax[1].get_yticks()
    ax[1].set_yticklabels([int(abs(tick)) for tick in ticks])
    ax[1].axhline(y=0, xmin=0, xmax=3, color="black", lw=0.75)

    ax[2].tick_params(bottom=False)
    ticks =  ax[2].get_yticks()
    ax[2].set_yticklabels([int(abs(tick)) for tick in ticks])
    ax[2].axhline(y=0, xmin=0, xmax=3, color="black", lw=0.75)

    ax[3].tick_params(bottom=False)
    ticks =  ax[3].get_yticks()
    ax[3].set_yticklabels([int(abs(tick)) for tick in ticks])
    ax[3].axhline(y=0, xmin=0, xmax=3, color="black", lw=0.75)

    ax[4].tick_params(bottom=False)
    ticks =  ax[4].get_yticks()
    ax[4].set_yticklabels([int(abs(tick)) for tick in ticks])
    ax[4].axhline(y=0, xmin=0, xmax=3, color="black", lw=0.75)

    ax[5].tick_params(bottom=False)
    ticks =  ax[5].get_yticks()
    ax[5].set_yticklabels([int(abs(tick)) for tick in ticks])
    ax[5].axhline(y=0, xmin=0, xmax=3, color="black", lw=0.75)

    #ax[6].tick_params(bottom=False)
    ticks =  ax[6].get_yticks()
    ax[6].set_yticklabels([int(abs(tick)) for tick in ticks])
    ax[6].axhline(y=0, xmin=0, xmax=3, color="black", lw=0.75)


    plt.subplots_adjust(wspace=0.4, hspace=0.6)
    # Put a legend on top of current axis
    ax[6].legend(loc='upper center', bbox_to_anchor=(0.45, -.45),
          fancybox=True, shadow=False, ncol=3, fontsize=12)

    #plt.setp(ax1.get_xticklabels(), fontsize=20)
    #plt.setp(ax1.get_yticklabels(), fontsize=20)
    plt.xticks(np.arange(4),labels, fontsize=8)
    ax[6].set(xlim=(-0.5, 2.5))

    """if(wordlist == "wikipedia"):
        ax[0].set(ylim=(0, 15))
        ax[1].set(ylim=(0, 15))
        ax[2].set(ylim=(0, 15))
        ax[3].set(ylim=(0, 15))
        ax[4].set(ylim=(0, 15))
        ax[5].set(ylim=(0, 15))
        ax[6].set(ylim=(0, 15))"""

    #plt.tight_layout()
           
    fig.savefig("longitudinal_%s.pdf"%(language))   # save the figure to file
    plt.close(fig)


def PlotTotalLongitudinalStacked(client_evolution_over_time,client_evolution_over_time_update_only,language):

    fig, ax = plt.subplots(7, 1, sharex=True, figsize=[6, 8])

    labels = ["Week 2", "Week 3", "Week 4"]
    x = np.arange(len(labels))
    width = 0.35

    #HK1
    additions = []
    additions.append(client_evolution_over_time["hk1"][1][0])
    additions.append(client_evolution_over_time["hk1"][2][0])
    additions.append(client_evolution_over_time["hk1"][3][0])

    additions_update_only = []
    additions_update_only.append(client_evolution_over_time_update_only["hk1"][1][0])
    additions_update_only.append(client_evolution_over_time_update_only["hk1"][2][0])
    additions_update_only.append(client_evolution_over_time_update_only["hk1"][3][0])

    removed = []
    removed.append(-client_evolution_over_time["hk1"][1][1])
    removed.append(-client_evolution_over_time["hk1"][2][1])
    removed.append(-client_evolution_over_time["hk1"][3][1])

    removed_update_only = []
    removed_update_only.append(-client_evolution_over_time_update_only["hk1"][1][1])
    removed_update_only.append(-client_evolution_over_time_update_only["hk1"][2][1])
    removed_update_only.append(-client_evolution_over_time_update_only["hk1"][3][1])

    ax[3].bar(x - width/2, additions_update_only, width, label='Added', color=colors[2])
    ax[3].bar(x - width/2, additions, width, bottom=additions_update_only, label='Added (search-only)', color=colors[0])
    ax[3].bar(x + width/2, removed_update_only, width, label='Removed', color=colors[3])
    ax[3].bar(x + width/2, removed, width, bottom=removed_update_only,label='Removed (search-only)', color=colors[1])
    

    ax[3].set(ylim=(-25, 25))
    ax[3].set_ylabel(ylabel='HK (1)',fontsize=14)
    #ax[3].set_title('Hong Kong (1)')

    #HK2
    additions = []
    #additions.append(client_evolution_over_time["hk2"][0][0])
    additions.append(client_evolution_over_time["hk2"][1][0])
    additions.append(client_evolution_over_time["hk2"][2][0])
    additions.append(client_evolution_over_time["hk2"][3][0])

    additions_update_only = []
    #additions.append(client_evolution_over_time["hk2"][0][0])
    additions_update_only.append(client_evolution_over_time_update_only["hk2"][1][0])
    additions_update_only.append(client_evolution_over_time_update_only["hk2"][2][0])
    additions_update_only.append(client_evolution_over_time_update_only["hk2"][3][0])

    removed = []
    #removed.append(client_evolution_over_time["hk2"][0][1])
    removed.append(-client_evolution_over_time["hk2"][1][1])
    removed.append(-client_evolution_over_time["hk2"][2][1])
    removed.append(-client_evolution_over_time["hk2"][3][1])

    removed_update_only = []
    #removed.append(client_evolution_over_time["hk2"][0][1])
    removed_update_only.append(-client_evolution_over_time_update_only["hk2"][1][1])
    removed_update_only.append(-client_evolution_over_time_update_only["hk2"][2][1])
    removed_update_only.append(-client_evolution_over_time_update_only["hk2"][3][1])

    ax[4].bar(x - width/2, additions_update_only, width, label='Added', color=colors[2])
    ax[4].bar(x - width/2, additions, width, bottom=additions_update_only, label='Added (search-only)', color=colors[0])
    ax[4].bar(x + width/2, removed_update_only, width, label='Removed', color=colors[3])
    ax[4].bar(x + width/2, removed, width, bottom=removed_update_only,label='Removed (search-only)', color=colors[1])
    
    
    ax[4].set(ylim=(-100, 100))
    ax[4].set_ylabel(ylabel='HK (2)',fontsize=14)
    #ax[4].set_title('Hong Kong (2)') 

    #Shanghai
    additions = []
    #additions.append(client_evolution_over_time["shanghai"][0][0])
    additions.append(client_evolution_over_time["shanghai"][1][0])
    additions.append(client_evolution_over_time["shanghai"][2][0])
    additions.append(client_evolution_over_time["shanghai"][3][0])

    additions_update_only = []
    #additions.append(client_evolution_over_time["shanghai"][0][0])
    additions_update_only.append(client_evolution_over_time_update_only["shanghai"][1][0])
    additions_update_only.append(client_evolution_over_time_update_only["shanghai"][2][0])
    additions_update_only.append(client_evolution_over_time_update_only["shanghai"][3][0])

    removed = []
    #removed.append(client_evolution_over_time["shanghai"][0][1])
    removed.append(-client_evolution_over_time["shanghai"][1][1])
    removed.append(-client_evolution_over_time["shanghai"][2][1])
    removed.append(-client_evolution_over_time["shanghai"][3][1])

    removed_update_only = []
    #removed.append(client_evolution_over_time["shanghai"][0][1])
    removed_update_only.append(-client_evolution_over_time_update_only["shanghai"][1][1])
    removed_update_only.append(-client_evolution_over_time_update_only["shanghai"][2][1])
    removed_update_only.append(-client_evolution_over_time_update_only["shanghai"][3][1])

    ax[5].bar(x - width/2, additions_update_only, width, label='Added', color=colors[2])
    ax[5].bar(x - width/2, additions, width, bottom=additions_update_only, label='Added (search-only)', color=colors[0])
    ax[5].bar(x + width/2, removed_update_only, width, label='Removed', color=colors[3])
    ax[5].bar(x + width/2, removed, width, bottom=removed_update_only,label='Removed (search-only)', color=colors[1])
    
    ax[5].set(ylim=(-500, 500))
    ax[5].set_ylabel(ylabel='Shanghai',fontsize=14)
    #ax[5].set_title('Shanghai')

    #Beijing (1)
    additions = []
    #additions.append(client_evolution_over_time["beijing-okoze"][0][0])
    additions.append(client_evolution_over_time["beijing-okoze"][1][0])
    additions.append(client_evolution_over_time["beijing-okoze"][2][0])
    additions.append(client_evolution_over_time["beijing-okoze"][3][0])

    additions_update_only = []
    #additions.append(client_evolution_over_time["beijing-okoze"][0][0])
    additions_update_only.append(client_evolution_over_time_update_only["beijing-okoze"][1][0])
    additions_update_only.append(client_evolution_over_time_update_only["beijing-okoze"][2][0])
    additions_update_only.append(client_evolution_over_time_update_only["beijing-okoze"][3][0])


    removed = []
    #removed.append(client_evolution_over_time["beijing-okoze"][0][1])
    removed.append(-client_evolution_over_time["beijing-okoze"][1][1])
    removed.append(-client_evolution_over_time["beijing-okoze"][2][1])
    removed.append(-client_evolution_over_time["beijing-okoze"][3][1])

    removed_update_only = []
    #removed.append(client_evolution_over_time["beijing-okoze"][0][1])
    removed_update_only.append(-client_evolution_over_time_update_only["beijing-okoze"][1][1])
    removed_update_only.append(-client_evolution_over_time_update_only["beijing-okoze"][2][1])
    removed_update_only.append(-client_evolution_over_time_update_only["beijing-okoze"][3][1])

    ax[1].bar(x - width/2, additions_update_only, width, label='Added', color=colors[2])
    ax[1].bar(x - width/2, additions, width, bottom=additions_update_only, label='Added (search-only)', color=colors[0])
    ax[1].bar(x + width/2, removed_update_only, width, label='Removed', color=colors[3])
    ax[1].bar(x + width/2, removed, width, bottom=removed_update_only,label='Removed (search-only)', color=colors[1])
    

    ax[1].set(ylim=(-25, 25))
    ax[1].set_ylabel(ylabel='Beijing (1)',fontsize=14)
    #ax[1].set_title('Beijing(1)')

    #Beijing (2)
    additions = []
    #additions.append(client_evolution_over_time["beijing-2"][0][0])
    additions.append(0)
    additions.append(0)
    additions.append(client_evolution_over_time["beijing-2"][3][0])

    additions_update_only = []
    #additions.append(client_evolution_over_time["beijing-2"][0][0])
    additions_update_only.append(0)
    additions_update_only.append(0)
    additions_update_only.append(client_evolution_over_time_update_only["beijing-2"][3][0])

    removed = []
    #removed.append(client_evolution_over_time["beijing-2"][0][1])
    removed.append(0)
    removed.append(0)
    removed.append(-client_evolution_over_time["beijing-2"][3][1])

    removed_update_only = []
    #removed.append(client_evolution_over_time["beijing-2"][0][1])
    removed_update_only.append(0)
    removed_update_only.append(0)
    removed_update_only.append(-client_evolution_over_time_update_only["beijing-2"][3][1])

    
    ax[2].bar(x - width/2, additions_update_only, width, label='Added', color=colors[2])
    ax[2].bar(x - width/2, additions, width, bottom=additions_update_only, label='Added (search-only)', color=colors[0])
    ax[2].bar(x + width/2, removed_update_only, width, label='Removed', color=colors[3])
    ax[2].bar(x + width/2, removed, width, bottom=removed_update_only,label='Removed (search-only)', color=colors[1])
    
    ax[2].set(ylim=(-25, 25))
    ax[2].set_ylabel(ylabel='Beijing (2)',fontsize=14)
    #ax[2].set_title('Beijing(2)')

    #Guangzhou
    additions = []
    #additions.append(client_evolution_over_time["guangzhou"][0][0])
    additions.append(0)
    additions.append(0)
    additions.append(client_evolution_over_time["guangzhou"][3][0])

    additions_update_only = []
    #additions.append(client_evolution_over_time["guangzhou"][0][0])
    additions_update_only.append(0)
    additions_update_only.append(0)
    additions_update_only.append(client_evolution_over_time_update_only["guangzhou"][3][0])

    removed = []
    #removed.append(client_evolution_over_time["guangzhou"][0][1])
    removed.append(0)
    removed.append(0)
    removed.append(-client_evolution_over_time["guangzhou"][3][1])

    removed_update_only = []
    #removed.append(client_evolution_over_time["guangzhou"][0][1])
    removed_update_only.append(0)
    removed_update_only.append(0)
    removed_update_only.append(-client_evolution_over_time_update_only["guangzhou"][3][1])

    ax[0].bar(x - width/2, additions_update_only, width, label='Added', color=colors[2])
    ax[0].bar(x - width/2, additions, width, bottom=additions_update_only, label='Added (search-only)', color=colors[0])
    ax[0].bar(x + width/2, removed_update_only, width, label='Removed', color=colors[3])
    ax[0].bar(x + width/2, removed, width, bottom=removed_update_only,label='Removed (search-only)', color=colors[1])
    

    ax[0].set(ylim=(-25, 25))
    ax[0].set_ylabel(ylabel='Guangzhou',fontsize=14)
    #ax[0].set_title('Guangzhou')

    #Pittsburgh
    additions = []
    #additions.append(client_evolution_over_time["pittsburgh"][0][0])
    additions.append(client_evolution_over_time["pittsburgh"][1][0])
    additions.append(client_evolution_over_time["pittsburgh"][2][0])
    additions.append(client_evolution_over_time["pittsburgh"][3][0])

    additions_update_only = []
    #additions.append(client_evolution_over_time["pittsburgh"][0][0])
    additions_update_only.append(client_evolution_over_time_update_only["pittsburgh"][1][0])
    additions_update_only.append(client_evolution_over_time_update_only["pittsburgh"][2][0])
    additions_update_only.append(client_evolution_over_time_update_only["pittsburgh"][3][0])

    removed = []
    #removed.append(client_evolution_over_time["pittsburgh"][0][1])
    removed.append(-client_evolution_over_time["pittsburgh"][1][1])
    removed.append(-client_evolution_over_time["pittsburgh"][2][1])
    removed.append(-client_evolution_over_time["pittsburgh"][3][1])

    removed_update_only = []
    #removed.append(client_evolution_over_time["pittsburgh"][0][1])
    removed_update_only.append(-client_evolution_over_time_update_only["pittsburgh"][1][1])
    removed_update_only.append(-client_evolution_over_time_update_only["pittsburgh"][2][1])
    removed_update_only.append(-client_evolution_over_time_update_only["pittsburgh"][3][1])

    print(additions_update_only)

    print(additions)
    ax[6].bar(x - width/2, additions_update_only, width, label='Added', color=colors[2])
    ax[6].bar(x - width/2, additions, width, bottom=additions_update_only, label='Added (search-only)', color=colors[0])
    ax[6].bar(x + width/2, removed_update_only, width, label='Removed', color=colors[3])
    ax[6].bar(x + width/2, removed, width, bottom=removed_update_only,label='Removed (search-only)', color=colors[1])
    

    ax[6].set(ylim=(-1500, 1500))
    #ax[6].set_title('[redacted], PA')
    ax[6].set_ylabel(ylabel='PA',fontsize=14)

    ax[0].spines['right'].set_visible(False)
    ax[0].spines['top'].set_visible(False)
    ax[0].spines['left'].set_visible(False)
    ax[0].spines['bottom'].set_visible(False)
    ax[1].spines['right'].set_visible(False)
    ax[1].spines['top'].set_visible(False)
    ax[1].spines['left'].set_visible(False)
    ax[1].spines['bottom'].set_visible(False)
    ax[2].spines['right'].set_visible(False)
    ax[2].spines['top'].set_visible(False)
    ax[2].spines['left'].set_visible(False)
    ax[2].spines['bottom'].set_visible(False)
    ax[3].spines['right'].set_visible(False)
    ax[3].spines['top'].set_visible(False)
    ax[3].spines['left'].set_visible(False)
    ax[3].spines['bottom'].set_visible(False)
    ax[4].spines['right'].set_visible(False)
    ax[4].spines['top'].set_visible(False)
    ax[4].spines['left'].set_visible(False)
    ax[4].spines['bottom'].set_visible(False)
    ax[5].spines['right'].set_visible(False)
    ax[5].spines['top'].set_visible(False)
    ax[5].spines['left'].set_visible(False)
    ax[5].spines['bottom'].set_visible(False)
    ax[6].spines['right'].set_visible(False)
    ax[6].spines['top'].set_visible(False)
    ax[6].spines['left'].set_visible(False)
    ax[6].spines['bottom'].set_visible(False)

    ax[0].tick_params(bottom=False)
    ticks =  ax[0].get_yticks()
    ax[0].set_yticklabels([int(abs(tick)) for tick in ticks])
    ax[0].axhline(y=0, xmin=0, xmax=3, color="black", lw=0.5)

    ax[1].tick_params(bottom=False)
    ticks =  ax[1].get_yticks()
    ax[1].set_yticklabels([int(abs(tick)) for tick in ticks])
    ax[1].axhline(y=0, xmin=0, xmax=3, color="black", lw=0.5)

    ax[2].tick_params(bottom=False)
    ticks =  ax[2].get_yticks()
    ax[2].set_yticklabels([int(abs(tick)) for tick in ticks])
    ax[2].axhline(y=0, xmin=0, xmax=3, color="black", lw=0.5)

    ax[3].tick_params(bottom=False)
    ticks =  ax[3].get_yticks()
    ax[3].set_yticklabels([int(abs(tick)) for tick in ticks])
    ax[3].axhline(y=0, xmin=0, xmax=3, color="black", lw=0.5)

    ax[4].tick_params(bottom=False)
    ticks =  ax[4].get_yticks()
    ax[4].set_yticklabels([int(abs(tick)) for tick in ticks])
    ax[4].axhline(y=0, xmin=0, xmax=3, color="black", lw=0.5)

    ax[5].tick_params(bottom=False)
    ticks =  ax[5].get_yticks()
    ax[5].set_yticklabels([int(abs(tick)) for tick in ticks])
    ax[5].axhline(y=0, xmin=0, xmax=3, color="black", lw=0.5)

    #ax[6].tick_params(bottom=False)
    ticks =  ax[6].get_yticks()
    ax[6].set_yticklabels([int(abs(tick)) for tick in ticks])
    ax[6].axhline(y=0, xmin=0, xmax=3, color="black", lw=0.5)


    plt.subplots_adjust(wspace=0.4, hspace=0.6)
    # Put a legend on top of current axis
    ax[6].legend(loc='upper center', bbox_to_anchor=(0.5, -.45),
          fancybox=True, shadow=False, ncol=2, fontsize=12)

    #plt.setp(ax1.get_xticklabels(), fontsize=20)
    #plt.setp(ax1.get_yticklabels(), fontsize=20)
    plt.xticks(np.arange(4),labels, fontsize=14)
    ax[6].set(xlim=(-0.5, 2.5))


    plt.tight_layout()
           
    fig.savefig("longitudinal_%s.pdf"%(language))   # save the figure to file
    plt.close(fig)



if __name__ == "__main__":

    """
    pairs = [
        ["chat","trad"],
        ["chat","non_trad"],
        ["wikipedia", "trad"],
        ["wikipedia", "non_trad"],
    ]

    for pair in pairs:
        
        wordlist = pair[0]
        language = pair[1]
        client_evolution_over_time = ComputeDifferencesOverTime(wordlist, language)
        PlotLongitudinal(client_evolution_over_time, wordlist)
    """

    client_evolution_over_time = ComputeTotalDifferencesOverTimeSearchOnly("non_trad")
    client_evolution_over_time_update_only = ComputeTotalDifferencesOverTimeUpdateOnly("non_trad")

    print("Results:")
    print(client_evolution_over_time)
    print()
    print(client_evolution_over_time_update_only)
    PlotTotalLongitudinalStacked(client_evolution_over_time,client_evolution_over_time_update_only,"non_trad")