import csv
import random
import os
import sys
import argparse


def gather_blocked_keywords(client, timestamp, wordlist):
    logs_folder = "../experiment_results/" 
    timestamp_folder = timestamp
    client_folder = "logs-" + client + "/experiment_logs/"
    client_logs = logs_folder + "/" + timestamp + "/" + client_folder

    blocked_keywords = set()


    #Gather blocked keywords
    for log_file in os.listdir(client_logs):

        if(wordlist in log_file):
            print("[*] " + client_logs + log_file)
            log_data = open(client_logs + log_file, 'r')
            csv_reader_full = csv.reader(log_data, delimiter=',')

            for n, row in enumerate(csv_reader_full):
                if(n == 0):
                    continue

                if(row[2] == "True"): 
                    blocked_keywords.add(row[0])

            log_data.close()

    #Write keywords found to be blocked to file        
    if not os.path.exists("blocked_keywords"):
        os.makedirs("blocked_keywords")
        
    results_file = open("blocked_keywords/blocked_%s_%s_%s.txt"%(client,timestamp,wordlist), 'w') 
    for k in blocked_keywords:
        results_file.write(k + "\n")
    results_file.close()












if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--client',
                    help='Client whose logs will be analysed.')
    parser.add_argument('-t', '--timestamp',
                    help='Folder timestamp to analyse.')
    parser.add_argument('-w', '--wordlist',
                    help='Wordlist used in probing.')

    args = parser.parse_args()

    
    gather_blocked_keywords(args.client, args.timestamp, args.wordlist)