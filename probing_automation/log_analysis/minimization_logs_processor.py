import csv
import random
import os
import sys

from py_stringmatching import OverlapCoefficient
from prettytable import PrettyTable
import time

def analyze_minimization_data(log_folder):
    
    #Create analysis log file
    if not os.path.exists("results/minimization_results/%s"%os.path.basename(log_folder)):
        os.makedirs("results/minimization_results/%s"%os.path.basename(log_folder))
    results_file = open("results/minimization_results/%s/results_%s.txt"%(os.path.basename(log_folder), os.path.basename(log_folder)), 'w') 

    results_file_sans_trad = open("results/minimization_results/%s/results_%s_non_trad.txt"%(os.path.basename(log_folder), os.path.basename(log_folder)), 'w') 
    
    #populate non trad list
    classification_file = open("../../wordlists/chat_list-wlc.csv", 'r')
    csv_reader_full = csv.reader(classification_file, delimiter=',') 
    non_trad_terms = set()

    for n, row in enumerate(csv_reader_full):
        if(n == 0):
            continue

        if(row[0] != "Han-t"):
            non_trad_terms.add(row[2])
    
    #####

    for log_file in os.listdir(log_folder + "/minimization_logs/"):
        if(log_file == ".DS_Store"):
            continue
        
        log_path = log_folder + "/minimization_logs/" + log_file
        log_data = open(log_path, 'r')
        csv_reader_full = csv.reader(log_data, delimiter='|')

        total_words = 0
        minimized_keywords = 0
        minimized_keywords_set = set()

        for n, row in enumerate(csv_reader_full):
            #Skip header
            if(n == 0 or n == 1):
                continue
            
            #Taking into account non_trad only
            if(row[len(row)-1] not in non_trad_terms):
                continue

            #Reached EOF
            if("End time" in row[0]):
                break

            total_words += 1

            #Multi decomposition
            if(len(row) >= 4):
                minimized_keywords += 1
                for i in range(0,len(row)-2):
                    if(row[i] in row[len(row)-1]):
                        minimized_keywords_set.add(row[i])

            #No decomposition
            elif(len(row) == 2 or len(row[0]) == len(row[2])):
                continue
            else:
                #Regular case with one triggering substring
                minimized_keywords += 1
                minimized_keywords_set.add(row[0])


              
        #Generate logs
        print("--------------------------------",file = results_file)
        print("Log file: %s"%(log_file),file = results_file)
        print("--------------------------------",file = results_file)
        print("Number of original keywords: %d"%total_words,file = results_file)
        print("Number of reduced keywords: %d"%minimized_keywords,file = results_file)
        print("",file=results_file)
        print("Number of minimized keywords: %d"%len(minimized_keywords_set),file = results_file)
        print("Set of minimized keywords:",file = results_file)
        print(minimized_keywords_set,file=results_file)
        print("",file = results_file)
        print("\n",file = results_file)

    results_file.close()

    
    

def process_keyword_minimization_logs(translate):
    
    minimization_log_folders = "../minimization_results/" 

    for timestamp_folder in os.listdir(minimization_log_folders):
        if(timestamp_folder == ".DS_Store"):
            continue
        
        print("########################################")
        print("Processing %s"%(minimization_log_folders + timestamp_folder))
        print("########################################")
        analyze_minimization_data(minimization_log_folders + timestamp_folder)



    
    

