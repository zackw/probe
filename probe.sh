#! /bin/sh

set -e

is_at_least_python_36 () {
    local ver
    ver=$("$1" -V) || return 1
    case "$ver" in
        (Python\ 3.[6789].* | Python\ 3.[1-9][0-9]*.*)
            return 0;;
        (*)
            return 1;;
    esac
}

if [ -z "$VIRTUAL_ENV" ]; then
    if [ ! -f ./venv/bin/activate ]; then
        # We need at least python 3.6.
        for candidate in python3.8 python3.7 python3.6 python3; do
            if is_at_least_python_36 $candidate; then
                $candidate -m venv venv
                break
            fi
        done
    fi
    . venv/bin/activate
fi
pip install -q --upgrade pip setuptools wheel
pip install -q -r requirements.txt
exec python -m probe "$@"
